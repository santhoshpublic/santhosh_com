<?php

/**
 * @file
 * Post update hooks for Ultimenu.
 */

/**
 * Clear cache to add new arguments for deprecated drupal_get_path().
 */
function ultimenu_post_update_for_drupal10() {
  // Empty hook to clear caches.
}

/**
 * Clear cache to remove new arguments for @extension.path.resolver.
 */
function ultimenu_post_update_path_resolver_service_pre_d9_3() {
  // Empty hook to clear caches.
}

/**
 * Clear cache to update schema for icon_class option.
 */
function ultimenu_post_update_added_icon_class_schema() {
  // Empty hook to clear caches.
}

/**
 * Clear cache to update UltimenuTree service paramater.
 */
function ultimenu_post_update_updated_ultimenu_tree_paramater() {
  // Empty hook to clear caches.
}
