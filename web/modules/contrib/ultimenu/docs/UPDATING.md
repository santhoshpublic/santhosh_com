***
***

# <a name="updating"> </a>UPDATING
If not updating from 1.x to 2.x, please ignore this section.

Ultimenu 2.x is a major rewrite to update for Drupal 8.6+, and add new features.
It may not be compatible with 1.x.
Ultimenu 2.x added few more services, so it may break the site temporarily
mostly due to the introduction of new services. If you do drush, this is no
issue. Running `drush cr`, `drush updb` and `drush cr` should do.

* Have backup routines.
* Test it out at a staging or DEV environment.
* Hit **Clear all caches** at
  [/admin/config/development/performance](/admin/config/development/performance)
  once this updated module is in place after `composer update`. This is
  important so that service or function changes do not interfere the update.
  Keep this page open, never re-load, and keep hitting that cute button if any
  issues.
* Run **/update.php**, or regular `drush cr`, `drush updb` and `drush cr`.

Note the recommended order above!
Read more [here](https://git.drupalcode.org/project/blazy/tree/docs/UPDATING.md?h=8.x-2.x)

The following are changes, in case you are updating from 1.x.


## NOTABLE CHANGES
- The flyouts no longer use `display:none`. Instead `visibility: hidden`. This
  should fix compatibility issues with lazy-loaded images without extra legs.
- Renamed **active-trail** LI class to **is-active-trail** to match core:
  https://www.drupal.org/node/2281785
- Renamed **js-ultimenu-** classes to **is-ultimenu-** so to print it in
  HTML directly not relying on JS, relevant for the new off-canvas menu.
- Added option to selectively enable ajaxified regions.
- Cleaned up CSS sample skins from old browser CSS prefixes. It is 2019.
- Added off-canvas menu to replace old sliding toggle approach.
- Split `ultimenu.css` into `ultimenu.hamburger.css` + `ultimenu.vertical.css`.
- Added support to have a simple iconized title, check out STYLING.


## FYI CHANGES
The following are just FYI, not really affecting the front-end, except likely
temporarily breaking the site till proper `drush cr` mentioned above.

1. UltimenuManager is split into 3 services: UltimenuSkin, UltimenuTool,
   UltimenuTree services for single responsibility, and to reduce complexity.
2. Ultimenu CSS files are moved from module **/skins** folder to **css/theme**.
3. Moved most logic to `#pre_render` to gain some performance.
4. Old `template_preprocess_ultimenu()` loop is also merged into `#pre_render`.
