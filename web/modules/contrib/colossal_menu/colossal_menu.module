<?php

/**
 * @file
 * Colossal Menu Hooks.
 */

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Render\Element;

/**
 * Implements hook_theme().
 */
function colossal_menu_theme() {
  return [
    'colossal_menu' => [
      'variables' => [
        'menu_name' => NULL,
        'items' => [],
        'attributes' => [],
      ],
    ],
    'colossal_menu_link' => [
      'render element' => 'elements',
    ],
  ];
}

/**
 * Prepares variables for Link templates.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_colossal_menu_link(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function colossal_menu_theme_suggestions_colossal_menu_link(array $variables) {
  $suggestions = [];
  $colossal_menu_link = $variables['elements']['#colossal_menu_link'];
  $suggestions[] = str_replace('-', '_', 'colossal_menu_link__' . $colossal_menu_link->bundle());
  return $suggestions;
}

/**
 * Implements hook_block_view_BASE_BLOCK_ID_alter().
 */
function colossal_menu_block_view_colossal_menu_block_alter(array &$build, BlockPluginInterface $block) {
  $build['#contextual_links']['colossal_menu'] = [
    'route_parameters' => [
      'colossal_menu' => $block->getDerivativeId(),
    ],
  ];
}
