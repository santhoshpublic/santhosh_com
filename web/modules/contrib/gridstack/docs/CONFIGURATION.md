***
***
# <a name="configuration"> </a>CONFIGURATION
Visit `/admin/structure/gridstack` to build a GridStack.  

1. **As Views style plugin:**
   * `/admin/structure/views`, and create a new page or block with
     GridStack style, and assign the designated optionset.
   * Use the provided sample to begin with, be sure to read its
     [README.md](/admin/help/gridstack_example).

2. **As field formatters:**
   * **Manage display** page, e.g.:
     `/admin/structure/types/manage/page/display`
   * Find **GridStack** formatters under **Manage display** for supported
     fields:
     Image, File entity reference, Media Entity, Paragraphs.
     With complex fields like Media Entity or Paragraphs, you can combine
     GridStack formatters with Slick formatters to build unique grid layout
     for the slider.

3. **As Bootstrap/ Foundation layouts:**
   * This requires any of _optional_ core Field Layout, Layout Builder, DS,
     Panelizer, or Widget modules, etc. to function.
   * Please refer to their documentations for better words.


## HOW TO
1. GridStack UI to enable the static grid support:

   `/admin/structure/gridstack/ui`

   Choose either Bootstrap, or Foundation under **Grid framework**.
   Only one grid framework can exist globally.
   Be sure to fill out the required Grid library to make it work with core
   Layout Builder. It won't be loaded at front-end, only needed at admin pages,
   so be sure you have a theme, or module, which loads it at front-end.

2. GridStack optionset collection page:

   `/admin/structure/gridstack`

   * Create, or clone existing **Default Bootstrap**, or **Default Foundation**.
     The required option: **Use static Grid Bootstrap/Foundation framework**.
     Disabled once saved to avoid issues.
   * Build static grid layout with floating boxes in mind, say
     **Boostrap Hero**.
     If a grid breaks, try putting them as nested grids within their own rows.
     + Leave **xl width** option empty if not desired, such as for Bootstrap 3.
       The **XL** grid class is only available at Bootstrap 4.
     + Clone existing optionsets to reduce steps.

3. Clear cache whenever updating/ editing existing layouts.
   * Don't always do this when all needed layouts are in place.

4. Assign the newly created layouts at any UI for Field, DS, Widget:
   * `/admin/structure/types/manage/article/display`
   * `/admin/structure/block`
   * Put relevant contents to each GridStack region.

5. Or visit `/admin/structure/views` to create Gridstack Views displays.


### RICH BOXES
Replace **Main stage/ image** if a node has one. It can be any entity reference,
like Block, etc. Use **block_field.module** for ease of block additions.

#### HOW TO RICH BOXES
1. Create a sticky (or far future created) node or two containing a Slick
   carousel, video, weather, time, donations, currency, ads, or anything as a
   block field.
2. Put it on the **Rich boxes** option.
   Be sure different from the **Main stage**.

While regular boxes are updated with new contents, these rich boxes may stay
the same, and sticky (Use Views with sort by sticky or desc by creation).
It doesn't limit to how many, as many as the available boxes can be rich boxes
as long as the node/ entity has the designated one.

### STAMPS
Stamp is _just_ a unique list, **Html list**, such as Latest news, blogs,
testimonials, etc. replacing one of the other boring boxes, including rich ones.


## TIPS
Use Responsive image for better mobile supports with various sizes.
With **Use CSS background** option enabled, those sizes don't matter much.

Use Blazy formatters to have Colorbox, Photobox, Photoswipe, or other
lightbox supports.
