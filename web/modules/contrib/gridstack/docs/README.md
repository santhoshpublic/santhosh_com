
# <a name="top"> </a>CONTENTS OF THIS FILE

 * [Introduction](#introduction)
 * [Requirements](#requirements)
 * [Installing library](#installing-library)
 * [Recommended modules](#recommended-modules)
 * [Installation/ Quick usage](#installation)
 * [Features](#features)
 * [Configuration](#configuration)
 * [Troubleshooting](#troubleshooting)
 * [FAQ](#faq)
 * [Roadmap](#roadmap)
 * [Maintainers](#maintainers)

***
***
# <a name="introduction"></a>INTRODUCTION

GridStack provides integration with **gridstack.js** as a dynamic layout creator
for both two-dimensional layouts (Masonry, Packery, Isotope, native browser
Grid Layout), and linear or one-dimensional layouts (Bootstrap/ Foundation,
etc.) with drag-and-drop. A few optional field formatters, and Views style
plugin.

***
## <a name="first"> </a>FIRST THINGS FIRST!
Read more at:
* [Github](https://git.drupalcode.org/project/blazy/-/blob/8.x-2.x/docs/README.md#first-things-first)
* [Blazy UI](/admin/help/blazy_ui#first)

***
***
# <a name="requirements"> </a>REQUIREMENTS
1. [Blazy 2.16+](https://www.drupal.org/project/blazy) since GridStack:2.10.
2. [GridStack](https://github.com/gridstack/gridstack.js) library, each
   version has different names, and structures, the assets are expected at:
   + _Version 4 or 5_:
     + `/libraries/gridstack/dist/gridstack-h5.js` (for admin UI)
     + `/libraries/gridstack/dist/gridstack-static.js` (for js frontend)
   + _Version 1.1 below_:
     + `/libraries/gridstack/dist/gridstack.min.js` (for js-driven + admin UI)
     + `/libraries/gridstack/dist/gridstack.jQueryUI.min.js` (admin UI)
     + `/libraries/gridstack/dist/gridstack.all.js` (Not used since 8.x-2.5)
     + `/libraries/gridstack/src/gridstack-poly.js`, etc. (for error debug.
       All non-minified JS files must be present in `src` folder at the same
       level as `dist` till the library change the source maps pointing to the
       same `dist` folder which also contain non-minified files. Dev only to
       debug errors.)

### Note:
* GridStack 8.x-2.10+ and DEV versions require library v5 and Blazy:2.16+.
* GridStack 8.x-2.9+ requires library v5 and Blazy:2.10+.
* GridStack 8.x-2.6+ requires a minimum v4+ or v5.
* GridStack 8.x-2.5 below require a minimum v1.1+ (deprecated).
* GridStack 8.x-2.4 below requires a maximum v0.5.3 below (deprecated).
* GridStack 8.x-1.0-beta4 below requires v0.2.5 (deprecated).

***
***
# <a name="installing-library"> </a>INSTALLING LIBRARY
Adjust the supported versions as per [REQUIREMENTS](#requirements) above, or
project home requirements for more updated info.
Applicable since `v4/v5`, installing libraries may be done in several ways:

* _With composer_:  
  `composer require npm-asset/gridstack:^5.1.1`  
  Assumed using `asset-packagist`, see [Blazy UI](/admin/help/blazy_ui) for
  setup samples. This will package the `gridstack/dist` folder automatically.
* _Without composer_:  
  `npm install --save gridstack@5.1.1`  
  This will package the `node_modules/gridstack/dist` folder automatically.
  Be sure to exclude `node_modules` folder when moving into `libraries` folder,
  just take out `gridstack` folder.
* _Manual download from Github_:  
  You may want to compile it yourself, see online tutorials on this.  

For old versions, in case the above result in different folder structures, check
out project home page for custom download url which may be removed later due to
no-longer being supported.  

***
***
# <a name="recommended-modules"> </a>RECOMMENDED MODULES
* [Outlayer](https://www.drupal.org/project/outlayer)

  **Brains and guts of a layout library**. Integrates Outlayer for layout
  libraries like Isotope, Masonry, Packery with Blazy and GridStack. Outlayer
  will make awesome GridStack layouts or custom-defined grids filterable,
  sortable, searchable.

## SIMILAR MODULES
[Mason](https://www.drupal.org/project/mason)

Both try to solve one problem: empty gaps within a compact grid layout.
Mason uses auto Fillers, or manual Promoted, options to fill in empty gaps.
GridStack uses manual drag and drop layout builder to fill in empty gaps.


***
***
# <a name="installation"> </a>INSTALLATION/ QUICK USAGE
* Install the module as usual, more info can be found on:  
  [Installing Drupal 8 Modules](https://drupal.org/node/1897420)
* `/admin/structure/modules`  
  Install the module under *Blazy* package including:  
  + GridStack UI (for layout CRUD),  
  + GridStack Example (for demo),  
  + GridStack Layouts (for usable layouts OOTB) to get up running quickly.  

  Uninstall them all if no longer used/ needed.
* `/admin/reports/status`  
  Verify the required library installed properly. Check permissions if persists.
* `/admin/structure/gridstack/ui`   
  Enable Bootstrap/ Foundation supports, etc.
* `/admin/structure/gridstack`  
  To manage or add initial layouts. Or skip for just defaults for now. Always
  clear cache whenever adding new layouts due to layout registrations are
  permanently cached. No need to keep clearing cache once layouts were
  registered unless any issue due to aggressive caching.
* `/node/123/layout` (Layout overrides).  
  `/admin/structure/types/manage/page/display/default/layout` (Layout defaults)  
  Requires an unlimited multi-value Media field available.
  Choose GridStack layouts at Layout Builder pages to have the custom styling as
  in the screenshots (WIP 8.x-2.6+ only). Be sure to have unlimited
  (multi-value) Media field first
  (e.g.: `/admin/structure/types/manage/page/fields`).


***
***
# <a name="features"></a>FEATURES
* Available as vanilla JavaScript (no jQuery) for front-end via *Use minimal
  GridStack* option. It cuts down 100Kb+ total.
* Layout Builder layout variants and stylizer with Media Library integration.
* Supports native browser CSS Grid layout as a replacement for library layout.
* Supports magazine layouts, identified by fixed heights.
* Supports static float layouts for Bootstrap, or Foundation, identified by
  auto heights. With Bootstrap 4 flexbox grid, limited magazine layout is
  possible with little CSS overrides.
* Supports optional core Layout Builder, DS, Widget modules.
* Responsive grid displays, layout composition, image styles, or multiple unique
  image styles per grid/box.
* Drag and drop layout builder.
* Lazyloaded inline images, or CSS background images with multi-styled images.
* Field formatters for Image, and fieldable entities like File Entity Reference,
  and core Media and Paragraphs integration. Specific to fieldable
  entities, best when containing core image using Blazy formatters with CSS
  background option enabled. Requires Blazy post Beta5+.
* Optional Views style plugin.
* Optional supports for Colorbox, Photobox, Blazy PhotoSwipe, or any lightbox
  supported by Blazy when using Blazy Views fields, or Blazy-related formatters
  from within Views.
* Modular and extensible layout engines.
* Modular and extensible skins.
* Easy captioning.
* Rich boxes (via **block_field.module**) to alternate boring images with a mix
  of rich boxes: Slick carousel, video, any potential block_field: currency,
  time, weather, ads, donations blocks, etc.
* Stamps (via Views **HTML list**)
* A few simple box layouts.
* All layout flavors are usable for Display Suite, or Layout Builder.
* Region or wrapper attributes, including their HTML tags, are configurable via
  Layout builder for both layout flavors.


***
***
# <a name="maintainers"> </a>MAINTAINERS/CREDITS
* [Gaus Surahman](https://www.drupal.org/user/159062)
* [Contributors](https://www.drupal.org/node/2672858/committers)
* CHANGELOG.txt for helpful souls with their patches, suggestions and reports.


## THIRD PARTY MATERIALS
**gridstack.css**

The version of gridstack.css included in this project is a derived from
gridstack.css 0.3.0-dev, and updated for v4 since module branch 2.6.

https://github.com/gridstack/gridstack.js

(c) 2014-2016 Pavel Reznikov, Dylan Weiss.
Currently maintained by Alain Dumesny and Dylan Weiss, originally created by
Pavel Reznikov.

It was licensed under the MIT license.

It has been modified by Gaus Surahman to work with Drupal.
The modified version provided modularity which is not accommodated by
the original file.


## READ MORE
See the project page on drupal.org:

[Gridstack module](https://drupal.org/project/gridstack)

See the GridStack JS docs at:

* [Gridstack at Github](https://github.com/gridstack/gridstack.js)
* [Gridstack website](https://gridstackjs.com)
