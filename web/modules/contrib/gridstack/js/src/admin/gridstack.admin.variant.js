/**
 * @file
 * Provides GridStack admin utilities for Layout Builder variants.
 *
 * Cheaper alt than full-blown class overrides via hook_element_plugin_alter
 * aside from the fact other modules might also race overriding that class.
 */

(function ($, Drupal, _win, _doc) {

  'use strict';

  var _id = 'gridstack';
  var _lb = 'layout-builder';
  var _isgs = 'is-gs';
  var _dgs = 'data-gs-';
  var _dgsv = _dgs + 'variant';
  var elBody = _doc.body;
  var idOnce = 'gsformvariant';
  var mounted = 'is-' + idOnce;
  var sBaseForm = '.form--' + _id + '-variant';
  var sForm = sBaseForm + ':not(.' + mounted + ')';
  var sBtnIcon = '.btn--save[data-icon]';
  var cGsSection = _isgs + '-section';
  var slb = '.' + _lb;
  var sBaseLbSection = slb + '__section';
  var sLbSection = sBaseLbSection + ':not(.' + cGsSection + ')';
  var sLbConfigure = slb + '__link--configure';
  var idVariantEdit = 'onVariantEdit';
  var sBtnEdit = '.btn--editor';
  var sBtnSave = '.btn--variant-save';
  var sBtnDup = '.btn--variant-duplicate';
  var sBtnDupLink = 'a.btn--editor-duplicate';
  var idVariantLabel = 'onVariantLabel';
  var sVariantLabel = '.form-text--label-variant';
  var _eVariantEdit = 'mousedown.' + idVariantEdit;
  var _eBlurVariantLabel = 'blur.' + idVariantLabel;
  var _cSaving = _isgs + '-lb-saving';
  var _click = 'click';
  var _mousedown = 'mousedown';

  /**
   * Reacts on button `Edit variant` AJAX events.
   *
   * @param {Event} e
   *   The event triggered by a `mousedown` event.
   */
  function fnOnVariantEdit(e) {
    var btn = e.target;
    var el = $.closest(btn, sLbSection);

    $.addClass(el, _isgs + '-editor-active');
  }

  /**
   * Reacts on blur event.
   *
   * @param {Event} e
   *   The blur event.
   */
  function fnOnBlurVariantLabel(e) {
    var input = e.target;
    var value = input.value;
    var form = $.closest(input, sBaseForm);
    var btn = $.find(form, sBtnDupLink);

    if ($.isElm(btn)) {
      var href = btn.href;

      if ($.contains(href, 'label')) {
        href = href.substring(0, href.indexOf('&label='));
      }

      href = href + '&label=' + Drupal.checkPlain(value);
      $.attr(btn, 'href', href);
    }
  }

  /**
   * GridStack form functions.
   *
   * @param {Element} form
   *   The GridStack form HTML element.
   */
  function process(form) {
    var $form = $(form);
    var eVariantJs = _click + '.gsvjs';

    /**
     * Reacts on button variant AJAX events.
     *
     * @param {Event} e
     *   The event triggered by a `mousedown` event.
     */
    function fnOnVariantAjax(e) {
      if (e.target === this) {
        var btn = e.target;
        var updateIcon = (parseInt(btn.dataset.gsUpdateIcon, 0) === 1) || false;
        var btnIcon = $.find(form, sBtnIcon);
        var elLbSection = $.closest(btn, sBaseLbSection);
        var action = btn.dataset.gsVariantAjax;
        var message = btn.dataset.gsVariantMessage;
        var vid = btn.dataset.gsVid;
        var elLink;

        if ($.isElm(btnIcon) && updateIcon) {
          btnIcon.click();
        }

        $.attr(elBody, _dgsv + '-body', vid);

        setTimeout(function () {
          // @todo, figure out a harder way, this is just too easy.
          if ($.isElm(elLbSection)) {
            if (action === 'duplicate') {
              vid = $.find(elLbSection, sBtnDup).dataset.gsVid;
              var elSave = $.find(elLbSection, sBtnSave);
              $.attr(elSave, 'gs-vid', vid);
              $.attr(elBody, _dgsv + '-body', vid);
            }

            $.attr(elLbSection, _dgsv + '-message', message);
            elLink = $.find(elLbSection, sLbConfigure);

            if ($.isElm(elLink)) {
              elLink.click();
            }
            $.addClass(elBody, _cSaving);
          }

          // 1500 is fair for duplicate AJAX operation. 3000 is to play safe.
        }, action === 'duplicate' ? 3000 : 2000);
      }
    }

    /**
     * Reacts on button `Update` AJAX events.
     *
     * @param {Event} e
     *   The event triggered by a `click` event.
     */
    function fnOnVariantJs(e) {
      e.preventDefault();
      var btnIcon = $.find(form, sBtnIcon);

      if (e.target === this) {
        if ($.isElm(btnIcon)) {
          btnIcon.click();
        }
      }
    }

    /**
     * Reacts on button `Update` AJAX events.
     *
     * @param {Event} e
     *   The event triggered by a `click` event.
     */
    function fnOnUpdateVariantTrigger(e) {
      e.preventDefault();

      var btn = e.target;
      var action = btn.dataset.gsVariantTrigger;
      var btnIcon = $.find(form, sBtnIcon);

      if (e.target === this) {
        if ($.isElm(btnIcon)) {
          btnIcon.click();
        }

        setTimeout(function () {
          var input = $.find(btn.parentNode, '> input[' + _dgsv + '-ajax="' + action + '"]');

          $.trigger(input, _mousedown);
        }, 100);
      }
    }


    $form.off(eVariantJs).on(eVariantJs, '.btn[' + _dgsv + '-js]', fnOnVariantJs);

    $.each($form.findAll('.btn[' + _dgsv + '-ajax]'), function (btn) {
      var action = btn.dataset.gsVariantAjax;
      var eVariantAJAX = _mousedown + '.gsvajax' + action;

      $(btn).off(eVariantAJAX).on(eVariantAJAX, fnOnVariantAjax);
    });

    var triggers = $form.findAll('.btn[' + _dgsv + '-trigger]');
    $.each(triggers, function (btn) {
      var action = btn.dataset.gsVariantTrigger;
      var eTrigger = _click + '.gsvtrigger' + action;

      $(btn).off(eTrigger).on(eTrigger, fnOnUpdateVariantTrigger);
    });

    $.addClass(form, mounted);
  }

  /**
   * Attaches gridstack behavior to HTML element .gridstack.is-gs-lb.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.gridStackAdminVariant = {
    attach: function (context) {

      elBody = _doc.body;
      var $body = $(elBody);

      $body.off(_eVariantEdit).on(_eVariantEdit, sBtnEdit, fnOnVariantEdit);
      $body.off(_eBlurVariantLabel).on(_eBlurVariantLabel, sVariantLabel, Drupal.debounce(fnOnBlurVariantLabel, 200));

      $.once(process, idOnce, sForm, context);
    },
    detach: function (context, settings, trigger) {
      if (trigger === 'unload') {
        var $body = $(_doc.body);
        $body.off(_eVariantEdit);
        $body.off(_eBlurVariantLabel);

        $.once.removeSafely(idOnce, sForm, context);
      }
    }
  };

})(dBlazy, Drupal, this, this.document);
