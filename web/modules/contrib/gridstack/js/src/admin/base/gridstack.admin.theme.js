/**
 * @file
 * Provides GridStack admin themes.
 */

(function (Drupal) {

  'use strict';

  /**
   * Theme function for a GridStack box.
   *
   * @param {Object} settings
   *   An object with the following keys: isNested, type.
   *
   * @return {HTMLElement}
   *   Returns a HTMLElement object.
   */
  Drupal.theme.gridStackBox = function (settings) {
    var type = settings.type || 'nested';
    var classes = 'gridstack__box box box--js grid-stack-item' + (type === 'nested' && settings.isNested ? ' box--nested is-box-nested' : '');
    var attributes = '';
    var tpl = '';

    tpl += '<div class="' + classes + '"' + attributes + '>';
    tpl += Drupal.theme('gridStackContent', settings);
    tpl += '</div>';

    return tpl;
  };

  /**
   * Theme function for a GridStack box content.
   *
   * @param {Object} settings
   *   An object with the following keys: isNested, type.
   *
   * @return {HTMLElement}
   *   Returns a HTMLElement object.
   */
  Drupal.theme.gridStackContent = function (settings) {
    var type = settings.type || 'nested';
    var useRegion = settings.isRegion || false;
    var placeholder = settings.regionPlaceholder || 'region';
    var tpl = '';

    tpl += '<div class="box__content grid-stack-item-content">';
    tpl += '<div class="btn-group btn-group--js">';
    tpl += '<button class="button btn btn--box btn--' + type + ' btn--remove" data-message="remove" data-type="' + type + '">&times;</button>';

    if (type === 'root') {
      if (settings.isNested) {
        tpl += '<button class="button btn btn--box btn--' + type + ' btn--add" data-message="add" data-type="' + type + '">+</button>';
      }
      // @todo no longer works since blazy:2.0 removed its custom breakpoints.
      // @todo else {
      // @todo   tpl += '<select class="form-select form-select--image-style" data-imageid="" id="" />';
      // @todo }
    }

    tpl += '</div>';

    if (settings.isNested) {
      // @todo  adding grid-stack classes error el.gridstack.onResizeHandler();.
      tpl += '<div class="gridstack gridstack--ui gridstack--nested is-gs-enabled is-gs-layout"></div>';
    }

    if (useRegion) {
      tpl += Drupal.theme('gridStackHtml5Autocomplete', {
        regionPlaceholder: placeholder
      });
    }

    tpl += '</div>';

    return tpl;
  };

  /**
   * Theme function for a GridStack autocomplete.
   *
   * @param {Object} settings
   *   An object with the following keys: regionPlaceholder.
   *
   * @return {HTMLElement}
   *   Returns a HTMLElement object.
   */
  Drupal.theme.gridStackAutocomplete = function (settings) {
    var placeholder = settings.regionPlaceholder || 'region';
    var tpl = '';

    tpl += '<div class="ui-widget ui-widget--region">';
    tpl += '<input class="form-text form-text--region" type="text" name="region" value="" size="20" maxlength="128" placeholder="' + placeholder + '">';
    tpl += '</div>';

    return tpl;
  };

  /**
   * Theme function for a GridStack HTML5 autocomplete.
   *
   * @param {Object} settings
   *   An object with the following keys: regionPlaceholder.
   *
   * @return {HTMLElement}
   *   Returns a HTMLElement object.
   */
  Drupal.theme.gridStackHtml5Autocomplete = function (settings) {
    var placeholder = settings.regionPlaceholder || 'region';
    var id = settings.id || '';
    var tpl = '';

    tpl += '<div class="ui-widget ui-widget--region">';
    tpl += '<input list="region' + id + '" autocomplete="off" class="form-text form-text--region" type="text" name="region" value="" size="20" maxlength="128" placeholder="' + placeholder + '">';
    tpl += '<datalist id="region' + id + '"></datalist>';
    tpl += '</div>';

    return tpl;
  };

})(Drupal);
