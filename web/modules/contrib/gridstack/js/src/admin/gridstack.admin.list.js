/**
 * @file
 * Provides GridStack admin utilities.
 */

(function ($, Drupal) {

  'use strict';

  var _idOnce = 'gslist';
  var _mounted = 'is-gsb-on';
  var _selector = '.b-lazy:not(.' + _mounted + ')';

  function process(el) {
    var url = el.dataset.src;
    if (!url) {
      url = el.src;
    }

    if (url && url.indexOf('?') < 0) {
      var date = new Date();
      $.attr(el, 'data-src', url + '?rand=' + date.getTime());
    }
    $.addClass(el, _mounted);
  }

  /**
   * Attaches gridstack behavior to HTML element .form--gridstack.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.gridStackAdminList = {
    attach: function (context) {
      $.once(process, _idOnce, _selector, context);
    }
  };

})(dBlazy, Drupal);
