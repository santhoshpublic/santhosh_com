/**
 * @file
 * Provides basic GridStack CSS rules utilities.
 */

(function (factory) {

  'use strict';
  var _win = window;

  // Browser globals (root is window).
  factory(_win.dBlazy, _win, _win.document);

})(function ($, scope, _doc) {

  'use strict';

  var GridStyle = {

    createStylesheet: function (id, parent) {
      var style = $.create('style');

      $.attr(style, 'type', 'text/css');
      $.attr(style, 'data-gs-style-id', id);

      if (style.styleSheet) {
        style.styleSheet.cssText = '';
      }
      else {
        style.appendChild(_doc.createTextNode(''));
      }

      if (!parent) {
        // Default to head.
        parent = _doc.getElementsByTagName('head')[0];
      }

      // parent.insertBefore(style, parent.firstChild);
      $.append(parent, style);
      return style.sheet;
    },

    removeStylesheet: function (id) {
      var style = $.find(_doc, 'STYLE[data-gs-style-id=' + id + ']');
      $.remove(style);
    },

    insertCSSRule: function (sheet, selector, rules, index) {
      if ($.isFun(sheet.insertRule)) {
        sheet.insertRule(selector + '{' + rules + '}', index);
      }
      else if ($.isFun(sheet.addRule)) {
        sheet.addRule(selector, rules, index);
      }
    },

    parseHeight: function (val) {
      var height = val;
      var heightUnit = 'px';
      if (height && $.isStr(height)) {
        var match = height.match(/^(-[0-9]+\.[0-9]+|[0-9]*\.[0-9]+|-[0-9]+|[0-9]+)(px|em|rem|vh|vw|%)?$/);
        if (!match) {
          throw new Error('Invalid height');
        }
        heightUnit = match[2] || 'px';
        height = parseFloat(match[1], 0);
      }
      return {
        height: height,
        unit: heightUnit
      };
    },

    defaults: function (target) {
      var sources = Array.prototype.slice.call(arguments, 1);

      $.each(sources, function (source) {
        for (var prop in source) {
          if ($.hasProp(source, prop) && (!$.hasProp(target, prop) || $.isUnd(target[prop]))) {
            target[prop] = source[prop];
          }
        }
      });

      return target;
    }

  };

  scope.GridStyle = GridStyle;

  /**
   * Initializing the grid style utilities.
   *
   * @param {object} opts
   *   The GridStatic options.
   * @param {HTMLElement} el
   *   The GridStatic HTML element.
   *
   * @return {function}
   *   The GridStatic instance.
   */
  GridStyle.init = function (opts, el) {
    var me = this;
    var prefix = opts.prefix || 'gridstack--style-';

    if (opts.id) {
      GridStyle.removeStylesheet(opts.id);
    }

    opts.id = prefix + (Math.random() * 100000).toFixed();

    // Insert style to parent (instead of 'head') to support WebComponent.
    // , el.parentNode
    me._styles = GridStyle.createStylesheet(opts.id);
    if (!$.isNull(me._styles)) {
      me._styles._max = 0;
    }
    return {
      id: opts.id,
      styles: me._styles
    };
  };

  return scope.GridStyle;

});
