/**
 * @file
 * Provides methods for both native CSS Grid and original library js layouts.
 */

(function ($, drupalSettings) {

  'use strict';

  $.gridstack = $.gridstack || {};

  /**
   * GridStack front-end public methods.
   *
   * @namespace
   */
  $.gridstack.fe = $.extend($.gridstack.base, {
    baseSettings: {
      mobileFirst: false,
      itemClass: 'gridstack__box'
    },
    gsSettings: {},
    config: {},
    dataset: [],
    range: function (start, stop, step) {
      if (stop == null) {
        stop = start || 0;
        start = 0;
      }
      if (!step) {
        step = stop < start ? -1 : 1;
      }
      var length = Math.max(Math.ceil((stop - start) / step), 0);
      var range = Array(length);
      for (var idx = 0; idx < length; idx++, start += step) {
        range[idx] = start;
      }
      return range;
    },

    breakpoint: function (which, el) {
      var me = this;
      var dataset = $.parse(el.dataset.gsColumns);
      dataset = $.isEmpty(dataset) ? me.columns : dataset;
      var keys = Object.keys(dataset);
      var ls = keys.length - 1;
      var xs = keys[0];
      var xl = keys[ls];
      var ww = me.windowWidth;
      var result;
      var mw = function (w) {
        return me.options.mobileFirst ? w <= ww : w >= ww;
      };

      var data = keys.filter(mw).map(function (v) {
        return which === 'width' ? v : dataset[v];
      })[me.options.mobileFirst ? 'pop' : 'shift']();

      if (which === 'column') {
        result = $.isUnd(data) ? dataset[ww >= xl ? xl : xs] : data;
      }
      else if (which === 'width') {
        result = $.isUnd(data) ? keys[ww >= xl ? ls : 0] : data;
      }

      return result;
    },

    updateColumn: function (el, e) {
      var me = this;
      var column = me.breakpoint('column', el);

      if (me.isValid(column)) {
        me.options.column = column;
        $.attr(el, 'data-gs-column', column);

        me.updateClasses(el);
      }
    },

    getData: function (el, e) {
      var me = this;
      var width = me.breakpoint('width', el);

      if (me.isValid(width)) {
        var dataset = $.isUnd(e) ? me.dataset : $.parse(el.dataset.gsData);
        return dataset ? dataset[width] : [];
      }
      return [];
    },

    updateRatioMultiple: function (el, grid, e) {
      var me = this;

      // Resizing needs fresh non-cached items to support multiple instances.
      var items = $.isUnd(e) ? me.$items : $.findAll(el, me.itemSelector);

      $.each(grid, function (item, i) {
        var box = items[i];

        if (me.isValid(box) && !$.isUnd(box)) {
          me.updateAttribute(box, item, el, e);
          me.updateRatio(box, el, e);
        }
      });
    },

    updateGrid: function (el, e) {
      var me = this;

      me.updateColumn(el, e);

      me.preUpdateGrid(el, e);

      var grid = me.getData(el, e);
      if (me.isValid(grid) && grid.length) {
        me.updateRatioMultiple(el, grid, e);
      }

      me.postUpdateGrid(el, e);
    },

    /**
     * Initializes the GridStack.
     *
     * @param {HTMLElement} el
     *   The gridstack HTML element.
     */
    prepare: function (el) {
      var me = this;
      var defaults = drupalSettings.gridstack || {};
      var dataset = el.dataset || {};

      me.config = $.parse(dataset.gsConfig);
      me.options = $.extend({}, defaults, me.baseSettings, me.gsSettings, me.config);
      me.columns = $.parse(dataset.gsColumns);
      me.dataset = $.parse(dataset.gsData);
      me.minW = me.config.minW || parseInt(dataset.gsMinW, 0) || me.minW;

      // Setup initials.
      me.initials(el);
    }

  });

})(dBlazy, drupalSettings);
