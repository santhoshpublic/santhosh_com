/**
 * @file
 * Provides loader for js-driven layout using GridStack JS library, or builtin.
 */

(function ($, Drupal) {

  'use strict';

  var _id = 'gridstack';
  var _nick = 'gs';
  var _isgs = 'is-' + _nick;
  var _idOnce = 'gsload';
  var _mounted = _isgs + '-on';
  var _selector = '.' + _id + '--' + _nick + ':not(.' + _mounted + ')';

  $.gridstack = $.gridstack || {};

  /**
   * GridStack front-end public methods.
   *
   * @namespace
   */
  $.gridstack.gs = $.extend({}, $.gridstack.fe || {}, {

    onClass: _nick,

    gsSettings: {
      // A trick to ignore height calculation with dummy sizer.
      placeholderClass: _id + '__sizer',
      // Will manually do 1 column = true.
      // float: true
      disableOneColumnMode: true,
      // Hard-code previously configurable settings due to deprecation.
      staticGrid: true,
      draggable: false,
      resizable: false,
      disableDrag: true,
      disableResize: true,
      alwaysShowResizeHandle: false,
      // If false grid will be static.
      // (default?: null - first available plugin will be used)
      ddPlugin: false
    },

    isGridStatic: function (el) {
      return $.hasClass(el, _id + '--static');
    },

    fixAspectRatio: function (el) {
      var me = this;
      var opts = me.options;
      var instance = me.$instance;
      var ch = opts.cellHeight;
      var ok = ch === 'auto' || (me.cellHeight >= ch && me.cellHeight < (ch * 2));
      var vm = me.vm > 0 ? (me.vm / 2) : 0;

      opts.dataset = me.getData(el);
      if (me.isValid(instance)) {
        if ($.isFun(instance.prepareNodes)) {
          instance.prepareNodes(opts.dataset);
        }
      }

      // Requires to determine the correct aspect ratio as otherwise weird.
      if (me.isEnabled(el)) {
        me.cellHeight = ok ? (me.cellHeight - vm) : ch;
        opts.cellHeight = me.cellHeight;

        // No need to change it within LB due to frequent AJAX reloads.
        if (!me.isLayoutBuilder && (me.isValid(instance) && me.isValid(instance._styles))) {
          instance.cellHeight(me.cellHeight);
        }
      }
    },

    updateAttribute: function (box, item, el, e) {
      var me = this;

      // Params: el, {x, y, w, h}.
      // For some reasons, instance.update is too slow to work with resizing.
      $.each(['x', 'y', 'w', 'h'], function (node, i) {
        $.attr(box, 'gs-' + node, item[i]);
      });

      var opts = {x: item[0], y: item[1], w: item[2], h: item[3]};
      me.$instance.update(box, opts);
    },

    postUpdateGrid: function (el, e) {
      var me = this;

      me.$instance.column(me.column(el));
    },

    destroy: function (el) {
      var me = this;

      me.$instance.destroy(false);

      $.addClass(el, _isgs + '-destroyed');
      $.removeAttr(el, 'style');
    },

    init: function (el) {
      var me = this;

      me.$el = el;

      me.prepare(el);

      // Unlike native Grid Layout, fixed aspect ratio earlier, before layouts.
      // Runs it before init to pass the options to gridstack.
      me.updateSizer(el);

      var opts = me.options;
      me.$instance = me.isGridStatic(el) ? GridStatic.init(opts, el) : GridStack.init(opts, el);

      me.buildOut(el);
      me.cleanUp(el);
    }

  });

  /**
   * Attaches behavior to HTML element identified by .gridstack--gs.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.gridstackGs = {
    attach: function (context) {

      var me = $.gridstack.gs;
      $.once(me.init.bind(me), _idOnce, _selector, context);
    },
    detach: function (context, settings, trigger) {
      if (trigger === 'unload') {
        $.once.removeSafely(_idOnce, _selector, context);
      }
    }
  };

})(dBlazy, Drupal);
