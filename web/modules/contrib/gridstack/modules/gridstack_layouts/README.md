
# INTRODUCTION
The GridStack Layouts can be used directly, or cloned as needed and made yours.
You can also copy this particular sub-module into your custom directory, and
rename any occurance of `gridstack_layouts` files and folder names and
`GridStack Layouts` to whatever you want, and start working with your
customization. And then uninstall this module to be replaced by yours.

Currently only provides layouts for Bootstrap 4. They can be converted to
Bootstrap 3 or Foundation just as easily. Only regions names will need
redefining since both don't have XL breakpoint.

## <a name="installation"> </a>INSTALLATION
1. Enable this module and `GridStack UI` module at `/admin/modules`
2. Visit `/admin/structure/gridstack/ui` to choose a one-dimensional layout
   engine, e.g.: Bootstrap4, etc.
3. The new layouts will be available at:
   * `/admin/structure/gridstack/`
   * `/admin/structure/gridstack/variant`
4. The layouts can be chosen at Layout Builder pages.
5. Add and edit layout variants at Layout Builder pages.

The config is put into `/config/optional` so it won't conflict your existing
custom GridStack layouts. They will only be installed if no similar names found.

The layout variant (`GridStackVariant`) is just a subset or extension of an
existing main layout (`GridStack`), currently only having the last breakpoint
(`xl` or `lg`, depending on the above-chosen layout engine). Basically,
Bootstrap4 and js-driven layouts support `xs, sm, md, lg, xl` breakponts, and
Bootstrap3 and Foundation support `sm, md, lg`.

You can implement `hook_gridstack_variant_applicable_breakpoints_alter` to add
more breakpoints just like the main layout if that won't confuse your editors.
Perhaps adding `md` and `lg` will do.


# <a name="recommended-modules"> </a>RECOMMENDED MODULES
* [Config update](https://drupal.org/project/config_update)

To verify and check out for any config left, visit:

`/admin/config/development/configuration/report/module/gridstack_layouts`

If all is clean, nothing to do.
If there are configs left there at first install, it is likely due to similar
config names exist. You can either rename yours, or leave the leftovers
untouched, no problem.

If you have awesome layouts, and would like to be included as part of this
module, feel free to submit a feature. TIA.
