<?php

namespace Drupal\gridstack;

use Drupal\blazy\Blazy;
use Drupal\blazy\BlazyDefault;
use Symfony\Component\Yaml\Yaml;

/**
 * Defines shared plugin default settings for field formatter and Views style.
 *
 * @see FormatterBase::defaultSettings()
 * @see StylePluginBase::defineOptions()
 *
 * @todo move style stuffs into plugins.
 */
class GridStackDefault extends BlazyDefault {

  /**
   * Defines root level.
   */
  const LEVEL_ROOT = 0;

  /**
   * Defines root item level.
   */
  const LEVEL_ROOT_ITEM = 1;

  /**
   * Defines nested level.
   */
  const LEVEL_NESTED = 2;

  /**
   * Defines nested item level.
   */
  const LEVEL_NESTED_ITEM = 3;

  /**
   * Defines dummy root selector.
   */
  const ROOT = ':root';

  /**
   * Defines dummy nested selector.
   */
  const NESTED = ':nested';

  /**
   * Defines a container.
   */
  const CONTAINER = 'Container';

  /**
   * Defines a region.
   */
  const REGION = 'Region';

  /**
   * {@inheritdoc}
   *
   * @todo enable post blazy:2.17, and remove dup from self::htmlSettings().
   * protected static $id = 'gridstacks';
   */

  /**
   * Returns common settings across plugins.
   */
  public static function commonSettings() {
    return [
      'gridnative' => FALSE,
      'skin'       => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseSettings() {
    return [
      'background' => FALSE,
      'layout'     => '',
      'optionset'  => 'default',
      'vanilla'    => FALSE,
    ] + self::commonSettings() + parent::baseSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function imageSettings() {
    return [
      'background'  => TRUE,
      'category'    => '',
      'stamp'       => '',
      'stamp_index' => 0,
    ] + parent::imageSettings() + self::baseSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function extendedSettings() {
    return self::imageSettings() + parent::extendedSettings();
  }

  /**
   * Returns view fields options for Views UI.
   */
  public static function viewFieldOptions() {
    return [
      'captions',
      'layouts',
      'images',
      'links',
      'titles',
      'classes',
      'overlays',
    ];
  }

  /**
   * Returns fixed settings not configurable via UI.
   */
  public static function fixedSettings() {
    return [
      'item_id'   => 'box',
      'nameshort' => 'gs',
      'namespace' => 'gridstack',
      'ratio'     => '',
      'blazy'     => TRUE,
      'lazy'      => 'blazy',
    ];
  }

  /**
   * Returns shared global form settings which should be consumed at formatters.
   */
  public static function cleanUiSettings() {
    return [
      'debug'      => FALSE,
      'framework'  => '',
      'no_classes' => FALSE,
      'library'    => '',
      'optimized'  => FALSE,
      'gridstatic' => FALSE,
    ];
  }

  /**
   * Returns shared global form settings which should be consumed at formatters.
   */
  public static function uiSettings() {
    return [
      'animationless' => FALSE,
      'editor_pos'    => '',
      'excludes'      => '',
      'fw_classes'    => '',
      'helpless'      => FALSE,
      'palettes'      => '',
      'palettes_pos'  => '',
      'skinless'      => FALSE,
    ] + self::cleanUiSettings();
  }

  /**
   * Returns style settings.
   */
  public static function styleSettings() {
    return [
      'responsive_image_style' => '',
      'alpha'                  => '',
      'opacity'                => '',
      'animations'             => [],
      'colors'                 => [],
      'extras'                 => [],
    ];
  }

  /**
   * Returns background color settings.
   */
  public static function rangeElements() {
    // @todo height sm-height md-height lg-height
    return ['opacity', 'alpha'];
  }

  /**
   * Returns background color settings.
   */
  public static function bgElements() {
    return array_merge(self::rangeElements(), ['rgba', 'bg']);
  }

  /**
   * Returns color settings.
   */
  public static function textElements() {
    return ['text', 'a', 'a:hover', 'h2', 'h3'];
  }

  /**
   * Returns color settings.
   */
  public static function colorElements() {
    return array_merge(['rgba', 'bg'], self::textElements());
  }

  /**
   * Returns style settings.
   */
  public static function styleElements() {
    return array_merge(self::bgElements(), self::textElements());
  }

  /**
   * Returns the region layout settings.
   */
  public static function regionSettings() {
    return [
      'attributes'      => '',
      'styles'          => [],
      'target_id'       => '',
      'wrapper'         => '',
      'wrapper_classes' => '',
      'row_classes'     => '',
      '_fullwidth'      => FALSE,
    ];
  }

  /**
   * Returns the main layout settings.
   */
  public static function layoutSettings() {
    return [
      'current_selection' => '',
      'regions'           => [],
      'field_name'        => '',
      'gid'               => '',
      'metadata'          => '',
      'vid'               => '',
      'vm'                => '',
    ] + self::commonSettings() + self::regionSettings();
  }

  /**
   * Returns the main wrapper Layout Builder select options.
   */
  public static function mainWrapperOptions() {
    return [
      'article' => 'Article',
      'aside'   => 'Aside',
      'main'    => 'Main',
      'footer'  => 'Footer',
      'section' => 'Section',
    ];
  }

  /**
   * Returns wrapper Layout Builder select options.
   */
  public static function regionWrapperOptions() {
    return self::mainWrapperOptions() + [
      'figure' => 'Figure',
      'header' => 'Header',
    ];
  }

  /**
   * Returns GridStack specific HTML or layout related settings.
   */
  public static function gridstacks() {
    return [
      'column'        => 12,
      'contentless'   => FALSE,
      'debug'         => FALSE,
      'ungridstack'   => FALSE,
      'id'            => '',
      'lightbox'      => '',
      'nested'        => FALSE,
      'rid'           => NULL,
      'root'          => TRUE,
      'use_framework' => FALSE,
    ];
  }

  /**
   * Returns HTML or layout related settings, none of JS to shutup notices.
   */
  public static function htmlSettings() {
    return [
      // @todo remove post 2.17:
      'gridstacks' => \blazy()->settings(self::values()),
      // @todo remove + self::gridstacks() after migration.
    ] + self::gridstacks()
      + self::commonSettings()
      + self::imageSettings()
      + self::fixedSettings()
      + parent::htmlSettings();
  }

  /**
   * Returns breakpoints.
   */
  public static function breakpoints() {
    return [
      'xs' => 'xsmall',
      'sm' => 'small',
      'md' => 'medium',
      'lg' => 'large',
      'xl' => 'xlarge',
    ];
  }

  /**
   * Returns region ID.
   */
  public static function regionId($id) {
    return 'gridstack_' . $id;
  }

  /**
   * Returns variant wrapper AJAX ID.
   */
  public static function variantWrapperId($id) {
    return $id ? 'gridstack-variant-wrapper-' . str_replace(['_', ':'], '-', $id) : '';
  }

  /**
   * Returns gridstack random id related to the each layout.
   */
  public static function gid($id) {
    return str_replace(['-', ':'], '_', $id);
  }

  /**
   * Returns gridstack layout id.
   */
  public static function layoutId($id) {
    return 'gridstack_' . $id;
  }

  /**
   * Returns theme properties.
   */
  public static function themeProperties() {
    return [
      'items' => [],
      'aside' => [],
      'bottom' => [],
      'preface' => [],
      'postscript' => [],
    ] + self::hashedProperties();
  }

  /**
   * Returns theme properties prefixed with hashtags.
   */
  public static function hashedProperties() {
    return [
      'attached' => [],
      'attributes' => [],
      'content_attributes' => [],
      'layout' => NULL,
      'variant' => NULL,
      'optionset' => NULL,
      'settings' => [],
      'wrapper_attributes' => [],
    ];
  }

  /**
   * Returns theme properties prefixed with hashtags.
   */
  public static function itemProperties() {
    return [
      'attributes' => [],
      'caption_attributes' => [],
      'content_attributes' => [],
      'delta' => 0,
      'item' => [],
      'settings' => [],
    ];
  }

  /**
   * Reverts the optionset to source.
   */
  public static function import($module, $key) {
    $config_factory = \Drupal::configFactory();
    $config_path = \gridstack()->getPath('module', $module) . '/config/install/gridstack.optionset.' . $key . '.yml';
    $data = Yaml::parseFile($config_path);
    $config_factory->getEditable('gridstack.optionset.' . $key)
      ->setData($data)
      ->save(TRUE);
  }

  /**
   * Returns a wrapper to pass tests, or DI where adding params is troublesome.
   *
   * @todo remove for Blazy::fileUrlGenerator post Blazy:2.6+.
   */
  public static function pathResolver() {
    return \Drupal::hasService('extension.path.resolver') ? \Drupal::service('extension.path.resolver') : NULL;
  }

  /**
   * Returns the commonly used path, or just the base path.
   *
   * @todo remove for Blazy::getPath post Blazy:2.6+ when min D9.3.
   */
  public static function getPath($type, $name, $absolute = FALSE): string {
    return \gridstack()->getPath($type, $name, $absolute);
  }

  /**
   * Returns a wrapper to pass tests, or DI where adding params is troublesome.
   *
   * @todo remove for Blazy::fileUrlGenerator post Blazy:2.6+.
   */
  public static function fileUrlGenerator() {
    return \gridstack()->service('file_url_generator');
  }

  /**
   * Returns a wrapper to pass tests, or DI where adding params is troublesome.
   *
   * @todo remove for Blazy::fileUrlGenerator post Blazy:2.6+.
   */
  public static function streamWrapperManager() {
    return \gridstack()->service('stream_wrapper_manager');
  }

  /**
   * Returns a wrapper to pass tests, or DI where adding params is troublesome.
   */
  public static function service($service) {
    return \gridstack()->service($service);
  }

  /**
   * Creates an absolute web-accessible URL string.
   *
   * @todo remove for Blazy::createUrl post Blazy:2.17.
   */
  public static function createUrl($uri): string {
    if ($gen = self::fileUrlGenerator()) {
      return $gen->generateAbsoluteString($uri);
    }

    $function = 'file_create_url';
    return is_callable($function) ? $function($uri) : '';
  }

  /**
   * Transforms an absolute URL of a local file to a relative URL.
   *
   * @todo remove for Blazy:transformRelative post Blazy:2.6+.
   */
  public static function transformRelative($uri, $style = NULL, array $options = []): string {
    return Blazy::transformRelative($uri, $style, $options);
  }

  /**
   * Returns svg-related field formatter settings.
   *
   * @todo remove post blazy:2.17.
   */
  public static function svgSettings() {
    return [
      'svg_inline' => FALSE,
      'svg_fill' => FALSE,
      'svg_sanitize' => TRUE,
      'svg_sanitize_remote' => FALSE,
      'svg_hide_caption' => FALSE,
      'svg_attributes' => '',
    ];
  }

  /**
   * Verify the settings.
   */
  public static function verify(array &$settings): void {
    $config = $settings['gridstacks'] ?? NULL;
    $settings['image_style'] = $settings['image_style'] ?? '';
    if ($manager = \gridstack()->service('gridstack.manager')) {
      $ui = $manager->getUiSettings();

      if (!$config) {
        // @todo remove merge.
        $settings += array_merge(self::htmlSettings(), $ui);
        $settings = array_merge($settings, self::fixedSettings());
        $config = $settings['gridstacks'];
      }

      if (!$config->get('ui')) {
        // @todo use $ui = $manager->configMultiple('gridstack.settings');
        $config->set('ui', $ui);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected static function options(): array {
    return ['ltrim' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  protected static function values(): array {
    $items = [];

    // @todo remove post blazy:2.17:
    $selfs = self::gridstacks() + self::fixedSettings();
    foreach ($selfs as $key => $value) {
      if (is_bool($value)) {
        if (strpos($key, 'use_') !== FALSE) {
          $key = str_replace('use_', '', $key);
          $items['use'][$key] = $value;
        }
        else {
          $key = ltrim($key, '_');
          $items['is'][$key] = $value;
        }
      }
      else {
        if (strpos($key, 'item_') !== FALSE) {
          $key = str_replace('item_', '', $key);
          $items['item'][$key] = $value;
        }
        $items[$key] = $value;
      }
    }
    // @todo return self::gridstacks() + self::fixedSettings();
    return $items;
  }

}
