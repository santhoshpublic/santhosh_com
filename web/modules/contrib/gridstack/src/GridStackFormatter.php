<?php

namespace Drupal\gridstack;

use Drupal\blazy\BlazyFormatter;

/**
 * Provides GridStack formatter.
 */
class GridStackFormatter extends BlazyFormatter implements GridStackFormatterInterface {

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'gridstack';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * {@inheritdoc}
   */
  public function buildSettings(array &$build, $items) {
    $this->hashtag($build);

    // Prepare integration with Blazy.
    $settings = &$build['#settings'];
    $this->verifySafely($settings);

    // Pass basic info to parent::buildSettings().
    parent::buildSettings($build, $items);

    $this->moduleHandler()->alter('gridstack_settings', $build, $items);
  }

  /**
   * {@inheritdoc}
   *
   * @todo remove at 3.x.
   */
  public function verify(array &$settings): void {
    parent::verify($settings);

    GridStackDefault::verify($settings);
  }

  /**
   * {@inheritdoc}
   */
  public function verifySafely(array &$settings, $key = 'blazies', array $defaults = []) {
    GridStackDefault::verify($settings);

    return parent::verifySafely($settings, $key, $defaults);
  }

}
