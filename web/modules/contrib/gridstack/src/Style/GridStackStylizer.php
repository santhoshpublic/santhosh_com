<?php

namespace Drupal\gridstack\Style;

use Drupal\Component\Utility\NestedArray;
use Drupal\gridstack\GridStackPluginManagerBase;

/**
 * Provides GridStack stylizer.
 */
class GridStackStylizer extends GridStackPluginManagerBase implements GridStackStylizerInterface {

  /**
   * {@inheritdoc}
   */
  protected static $path = 'Plugin/gridstack/stylizer';

  /**
   * {@inheritdoc}
   */
  protected static $interface = 'Drupal\gridstack\Style\GridStackStylizerPluginInterface';

  /**
   * {@inheritdoc}
   */
  protected static $annotation = 'Drupal\gridstack\Annotation\GridStackStylizer';

  /**
   * {@inheritdoc}
   */
  protected static $key = 'gridstack_stylizer';

  /**
   * The admin regions.
   *
   * @var array
   */
  protected $regions = [];

  /**
   * The loaded stylizer plugin.
   *
   * @var array
   */
  protected $loadStyle;

  /**
   * The style plugin.
   *
   * @var \Drupal\gridstack\Plugin\gridstack\stylizer\Style
   */
  protected $style;

  /**
   * The builder plugin.
   *
   * @var \Drupal\gridstack\Plugin\gridstack\stylizer\Builder
   */
  protected $builder;

  /**
   * The form plugin.
   *
   * @var \Drupal\gridstack\Plugin\gridstack\stylizer\Form
   */
  protected $form;

  /**
   * {@inheritdoc}
   */
  public function style(array $configuration = [], $reload = FALSE): ?object {
    if (!isset($this->style) || $reload) {
      $this->style = $this->loadStyle('style', $configuration, $reload);
    }
    return $this->style;
  }

  /**
   * Sets the style plugin.
   */
  private function setStyle(array $configuration, $reload = FALSE): self {
    $this->style = $this->loadStyle('style', $configuration, $reload);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStyle($key, array $settings) {
    return $this->style()->getStyle($key, $settings);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguredStyle($key, array $settings): ?object {
    return $this->style()->getStyle($key, $settings);
  }

  /**
   * {@inheritdoc}
   */
  public function builder(array $configuration = [], $reload = FALSE): ?object {
    if (!isset($this->builder) || $reload) {
      $this->builder = $this->loadStyle('builder', $configuration, $reload);
    }
    return $this->builder;
  }

  /**
   * Sets the builder plugin.
   */
  private function setBuilder(array $configuration = [], $reload = FALSE) {
    $this->builder = $this->loadStyle('builder', $configuration, $reload);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $configuration = [], $reload = FALSE): ?object {
    if (!isset($this->form) || $reload) {
      $this->form = $this->loadStyle('form', $configuration, $reload);
    }
    return $this->form;
  }

  /**
   * {@inheritdoc}
   */
  public function setForm(array $configuration = [], $reload = FALSE): self {
    $this->form = $this->loadStyle('form', $configuration, $reload);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function modifyItem(
    $delta,
    array &$settings,
    array &$content,
    array &$attributes,
    array &$content_attributes,
    array $regions = []
  ): void {
    $config = $settings['gridstacks'];
    $rid = $config->get('rid', -1);
    $ipe = $config->is('ipe');

    if (!$ipe && $config->is('contentless')) {
      $content['box'] = [];
    }

    // Layout Builder only output for granted users.
    if ($ipe && isset($this->regions[$rid])) {
      $this->builder()->adminAttributes($content['box'], $content_attributes, $settings, $this->regions ?: $regions);
    }

    // Provides background media to support contentless, if any.
    $settings['delta'] = $delta;
    if ($media = $this->style()->buildMedia($content_attributes, $settings)) {
      $content['box']['preface'] = $media;
      $content['box']['preface']['#weight'] = -100;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(
    array &$element,
    array &$attributes,
    array &$settings,
    $optionset
  ): void {
    $config = $settings['gridstacks'];
    // Provides configurable attributes and background media if so configured.
    // Runs before ::containerAttributes to pass the media settings.
    $this->setStyle($settings);
    if ($render = $this->style()->buildMedia($attributes, $settings)) {
      $element['#preface']['media'] = $render;
    }

    // Provides admin utilities.
    if ($config->is('ipe')) {
      $this->setBuilder($settings);
      $this->regions = $this->builder()->regions($element);

      if ($links = $this->builder()->getVariantEditor($settings, $optionset)) {
        $pos = $this->config('editor_pos') == 'bottom' ? '#bottom' : '#aside';
        $element[$pos]['layout_editor'] = $links;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function rootStyles(array &$element, array $styles, array $settings) {
    $config = $settings['gridstacks'];
    if ($rules = $this->style()->parseStyles($styles, TRUE)) {
      $css = implode('', reset($rules));
      $element['#attached']['html_head'][] = [
        ['#tag' => 'style', '#value' => $css, '#weight' => 1],
        'gridstack-style-' . key($rules),
      ];

      if ($config->is('ipe')) {
        $element['#attributes'] = $element['#attributes'] ?? [];
        $this->builder()->rootAttributes($element['#attributes'], $styles);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function styles(array &$attributes, array $settings): array {
    $config = $settings['gridstacks'];
    $styles = $this->style()->getStyles($settings);
    if ($styles && $data = $this->style()->getSelector($settings, '', $styles)) {
      // Only inline it to DOM element if at LB edit pages.
      if ($config->is('ipe')) {
        $this->builder()->inlineAttributes($attributes, $data);
      }
      return $data;
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function attributes(array &$attributes, array $settings): void {
    $this->style()->attributes($attributes, $settings);
  }

  /**
   * {@inheritdoc}
   */
  public function getInternalClasses(): array {
    return $this->style()->getInternalClasses();
  }

  /**
   * {@inheritdoc}
   */
  public function getMergedClasses($flatten = FALSE, array $framework_classes = []): array {
    $cid = $flatten ? 'gridstack_classes_flattened' : 'gridstack_classes_grouped';
    if ($data = $this->getCachedData($cid)) {
      return $data;
    }

    $output = $context = [];
    if ($classes = $this->mergeFrameworkAndInternalClasses($framework_classes)) {
      $grouped = $ungrouped = [];
      foreach ($classes as $group => $data) {
        $grouped[$group] = $data;
        $items = [];
        foreach ($data as $datum) {
          $items[] = $datum;
        }

        $ungrouped = NestedArray::mergeDeep($ungrouped, $items);
      }

      // Do not use array_unique() here, was processed per group instead.
      $output = $flatten ? $ungrouped : $grouped;
      $context = [
        'cid' => $cid,
        'grouped' => $grouped,
        'ungrouped' => $ungrouped,
        'flatten' => $flatten,
      ];
    }

    return $this->getCachedData(
      $cid,
      $output,
      TRUE,
      'gridstack_classes_preset',
      $context
    );
  }

  /**
   * {@inheritdoc}
   */
  public function attach(array &$load, array $attach, $config): void {
    if ($config->get('use.stylizer')) {
      $load['library'][] = 'gridstack/stylizer';

      $this->checkAttachments($load, $attach, $config);
    }
  }

  /**
   * Merges preset classes with the custom defined ones.
   */
  private function mergeFrameworkAndInternalClasses(array $classes = []): array {
    $internals = $this->getInternalClasses();
    $classes = $classes ? NestedArray::mergeDeep($internals, $classes) : $internals;
    if ($fw_classes = $this->config('fw_classes')) {
      $fw_classes = array_map('trim', explode("\n", $fw_classes));
      foreach ($fw_classes as $fw_class) {
        $fw_class = strip_tags($fw_class);
        if (strpos($fw_class, '|') !== FALSE) {
          [$group, $group_class] = array_pad(array_map('trim', explode("|", $fw_class, 2)), 2, NULL);
          $group_classes = array_map('trim', explode(" ", $group_class));
          $new_group = [];
          foreach ($group_classes as $group_class) {
            $new_group[] = $group_class;
          }

          $new_group = isset($classes[$group]) ? NestedArray::mergeDeep($classes[$group], $new_group) : $new_group;
          $classes[$group] = array_unique($new_group);
        }
      }
    }
    return $classes;
  }

  /**
   * {@inheritdoc}
   */
  private function loadStyle(
    $id = 'style',
    array $configuration = [],
    $reload = FALSE
  ) {
    if (!isset($this->loadStyle[$id]) || $reload) {
      if (!isset($configuration['gridstacks'])) {
        $configuration['gridstacks'] = \blazy()->settings();
      }
      $this->loadStyle[$id] = $this->load($id, $configuration);
    }
    return $this->loadStyle[$id];
  }

}
