<?php

namespace Drupal\gridstack\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a GridStackStylizer item annotation object.
 *
 * @Annotation
 */
class GridStackStylizer extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
