<?php

namespace Drupal\gridstack\Plugin\views\style;

use Drupal\blazy\Views\BlazyStylePluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gridstack\GridStackDefault as Defaults;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * GridStack style plugin.
 */
class GridStackViews extends BlazyStylePluginBase implements GridStackViewsInterface {

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'gridstack';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $captionId = 'caption';

  /**
   * The gridstack service manager.
   *
   * @var \Drupal\gridstack\GridStackManagerInterface
   */
  protected $manager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->manager = $container->get('gridstack.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function manager() {
    return $this->manager;
  }

  /**
   * {@inheritdoc}
   */
  public function admin() {
    return \Drupal::service('gridstack.admin');
  }

  /**
   * Overrides parent::buildOptionsForm().
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $definition = $this->getDefinedFormScopes();
    $this->buildSettingsForm($form, $definition);
  }

  /**
   * Overrides StylePluginBase::render().
   */
  public function render() {
    $settings = $this->buildSettings();
    $elements = [];

    foreach ($this->renderGrouping($this->view->result, $settings['grouping']) as $rows) {
      $settings = array_filter($settings, function ($value) {
        return ($value !== NULL && $value !== '' && $value !== []);
      });

      $items = $this->buildElements($settings, $rows);

      // Supports lightbox gallery if using Blazy formatter.
      $build = ['items' => $items];
      $this->checkBlazy($settings, $build, $rows);

      $build['#settings'] = $settings;

      $elements = $this->manager->build($build);
      unset($build);
    }

    return $elements;
  }

  /**
   * Returns gridstack contents.
   */
  public function buildElements(array $settings, $rows) {
    $build = [];
    $stamp_index = $settings['stamp_index'] ?? -1;
    $_image = $settings['image'] ?? NULL;

    foreach ($rows as $delta => $row) {
      $sets = $settings;
      $this->view->row_index = $delta;

      $box = $box['#attributes'] = [];
      $type = '';

      // Adds box type where image can be anything, not only image.
      if ($_image && $this->getField($delta, $_image)) {
        $sets['type'] = $type = $row->_entity->hasField($_image)
          ? $row->_entity
            ->getFieldDefinition($_image)
            ->getFieldStorageDefinition()
            ->getType()
          : '';
      }

      $sets['delta'] = $delta;
      $blazy = $sets['blazies']->reset($sets);
      $blazy->set('delta', $delta);

      $config = $this->manager->reset($sets, TRUE);
      $config->set('box.type', $type)
        ->set('delta', $delta);

      $box['#settings'] = $sets;

      // Use Vanilla gridstack if so configured, ignoring GridStack markups.
      if (!empty($sets['vanilla'])) {
        $box[static::$itemId] = $this->view->rowPlugin->render($row);
      }
      else {
        // Build individual row/element contents.
        $this->buildElement($box, $row, $delta);
      }

      // Allows extending contents without overriding the entire loop method.
      $this->buildElementExtra($box, $row, $delta);

      // Extracts stamp from existing box.
      $stamp = [];
      if ($delta == $stamp_index && !empty($box['stamp'])) {
        $stamp = $box['stamp'];
        $setstamp = &$stamp[$stamp_index]['#settings'];
        $setstamp['type'] = $type = 'stamp';
        $setstamp['gridstacks']->reset($setstamp, 'gridstacks')
          ->set('box.type', $type)
          ->set('delta', $delta);

        unset($box['stamp']);
      }

      // Build gridstack items.
      $build[] = $box;

      // Inserts stamp to the array.
      if ($delta == $stamp_index && !empty($stamp)) {
        array_splice($build, $stamp_index, 0, $stamp);
      }

      unset($box);
    }

    unset($this->view->row_index);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildElement(array &$box, $row, $delta) {
    $this->manager->hashtag($box);

    $settings = &$box['#settings'];
    $config = $settings['gridstacks'];
    $use_category = TRUE;

    // Add row/ item classes.
    if (!empty($settings['class'])) {
      $classes = $this->getFieldString($row, $settings['class'], $delta);
      if (!empty($classes[$delta])) {
        $box['#attributes']['class'][] = $classes[$delta];
      }
    }

    // Adds the rich box named overlay replacing the main stage to alternate
    // boring images with a mix of rich boxes: Slick carousel, any potential
    // block_field: video, currency, time, weather, ads, donations blocks, etc.
    if (!empty($settings['overlay'])
      && $this->getField($delta, $settings['overlay'])) {
      $box[static::$itemId] = $this->getFieldRendered($delta, $settings['overlay']);
      $settings['type'] = $type = 'rich';
      $config->set('box.type', $type);
      $use_category = FALSE;

      // As this takes over the entire box contents, nullify extra unused works.
      foreach (['caption', 'image', 'link', 'overlay', 'title'] as $key) {
        $settings[$key] = '';
      }
    }

    // Adds stamp, such as HTML list of latest news, members, testimonials, etc.
    $stamp_index = $settings['stamp_index'] ?? -1;
    if ($delta == $stamp_index && !empty($settings['stamp'])) {
      [$view_id, $display_id] = explode(":", $settings['stamp'], 2);

      $box['stamp'][$stamp_index] = [
        static::$itemId => views_embed_view($view_id, $display_id),
        '#settings'     => $settings,
        '#attributes'   => $box['#attributes'] ?? [],
      ];
    }

    // Adds category.
    if ($use_category
      && !empty($settings['category'])
      && $this->getField($delta, $settings['category'])) {
      $box[static::$captionId]['category'] = $this->getFieldRendered($delta, $settings['category']);
    }

    // Overrides parent::buildElement().
    parent::buildElement($box, $row, $delta);
  }

  /**
   * Returns extra row/ element content such as Isotope filters, sorters, etc.
   */
  public function buildElementExtra(array &$box, $row, $delta) {
    // Do nothing, let extender do their jobs.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = ['stamp' => ['default' => '']];
    foreach (Defaults::extendedSettings() as $key => $value) {
      $options[$key] = ['default' => $value];
    }
    return $options + parent::defineOptions();
  }

  /**
   * Returns the defined scopes for the current form.
   */
  protected function getDefinedFormScopes(array $extra_fields = []) {
    // Pass the common field options relevant to this style.
    $fields = Defaults::viewFieldOptions();
    $fields = array_merge($fields, $extra_fields);

    // Fetches the returned field definitions to be used to define form scopes.
    $definition = $this->getDefinedFieldOptions($fields);
    $definition['_views'] = TRUE;

    return $definition;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildSettings() {
    $settings = parent::buildSettings();
    $blazies  = $this->manager->verifySafely($settings);

    // @todo remove post blazy:2.17.
    $blazies->set('item.id', static::$itemId)
      ->set('item.prefix', static::$itemPrefix)
      ->set('item.caption', static::$captionId)
      ->set('namespace', static::$namespace)
      ->set('is.noratio', TRUE);

    // Prepare needed settings to work with.
    $settings['caption'] = array_filter($settings['caption']);
    $settings['ratio'] = '';

    return $settings;
  }

  /**
   * Build the Gridstack settings form.
   */
  protected function buildSettingsForm(&$form, &$definition) {
    $count = empty($definition['captions']) ? 0 : count($definition['captions']);
    $definition['captions_count'] = $count;
    $definition['stamps'] = $this->getViewsAsOptions('html_list');

    $this->admin()->buildSettingsForm($form, $definition);

    $title = '<p class="form__header form__title form-item--lead form-item--fullwidth">';
    $title .= $this->t('Check Vanilla if using content/custom markups, not fields. <small>See it under <strong>Format > Show</strong> section. Otherwise Gridstack markups apply which require some fields added below.</small>');
    $title .= '</p>';

    $form['opening']['#markup'] .= $title;
    $form['image']['#description'] .= ' ' . $this->t('Be sure to UNCHECK "Use field template" (by default already UNCHECKED) to have it work for Blazy lazyloading. Use Blazy formatters for relevant features such as Colorbox/Photobox/Photoswipe, etc., or multimedia supports.');
  }

}
