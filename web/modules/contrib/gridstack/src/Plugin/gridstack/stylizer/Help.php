<?php

namespace Drupal\gridstack\Plugin\gridstack\stylizer;

/**
 * Provides the help.
 *
 * @GridStackStylizer(
 *   id = "help",
 *   label = @Translation("Help")
 * )
 */
class Help extends MediaForm {

  /**
   * Returns the help element.
   */
  protected function helpElement() {
    $element = [
      '#type'       => 'details',
      '#open'       => FALSE,
      '#title'      => $this->t('Help?'),
      '#attributes' => ['class' => ['form-wrapper--help']],
      '#weight'     => 60,
    ];

    $items = [];
    $items[] = $this->t('Save the form first before working.');
    $items[] = $this->t('Create one multi-value unlimited <code>Media</code> field containing <code>Image, Video or Remote Video</code> bundles right on this entity/ content type before using the Media Library. Then select it under <code>Global settings > Main settings</code>.');
    $items[] = $this->t('Warning! Avoid animation options at large regions (containers) due to high CPU consumption. They are best for small regions/ containers.');
    $items[] = $this->t('Avoid <code>Viewport Height (VH 100)</code> or any fixed height, unless desired. Use <code>Min Height</code> instead.');
    $items[] = $this->t('Use <code>Min Height</code> at <code>Preset classes</code> if the image/ media is collapsed.');
    $items[] = $this->t('Select <code>Black (#000000)</code> to disable color.');
    $items[] = $this->t('Avoid color-related options at <code>Preset classes</code> to make custom <code>Styles</code> work.');
    $items[] = $this->t('The layout editor is two-dimensional. Unless for two-dimensional, it is not _always applicable to one-dimensional layouts. Understanding the principal difference should reduce confusion. Shortly, if the output is different from preview, adjust it accordingly.');
    $items[] = $this->t('Unless uploading images, no need to keep saving the form. Click each <code>+ Styles</code> button to select regions without AJAX once the modal is open instead. Once done, be sure to hit <code>Update</code> button, or lose all the stylings.');
    $items[] = $this->t('Anything under <code>Styles</code> requires the provided Media Library Image or Media, not blocks, for now.');
    $items[] = $this->t('If a layout variant is added or changed drastically (e.g. 12 columns becomes 2 or 3), be sure to re-sync, or re-save, this modal form (hit <code>Update</code> button) _only if any display issue. The reason, some options require 12 columns like EtE, etc.');
    $items[] = $this->t('If a layout variant is messed up for a reason, simply revert, or delete it.');
    $items[] = $this->t('<b>Known bugs</b>: <br>- when editing a region (not the global container), it must have a red outline. If not, try re-clicking <code>+ Styles</code> button. <br>- <b>Save as New</b> is still chaotic, not generating icon, not directly saves the label. Needs re-adding label, then <b>Update icon</b> > <b>Save</b> or <b>Apply</b>.');

    $element['help'] = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
    return $element;
  }

}
