<?php

namespace Drupal\gridstack\Plugin\gridstack\stylizer;

/**
 * Provides the background color styles.
 *
 * @GridStackStylizer(
 *   id = "background",
 *   label = @Translation("Background")
 * )
 */
class Background extends Color {

  /**
   * Returns the background colors grouped by the given key, w/o text colors.
   */
  protected function getBackgroundColor(array $settings, $with_text = TRUE) {
    $build = [];

    if ($this->getColors($settings)) {
      $rgb    = 'bg';
      $rgba   = 'rgba';
      $text   = 'text';
      $data   = $this->getSelector($settings);
      $key    = $data['selector'] ?? '';
      $bg_key = $data['bg_selector'] ?? '';

      // Background color and text color have the same selector, hence grouped.
      if ($key && ($this->getColor($rgba, $settings) || $this->getColor($rgb, $settings) || $this->getColor($text, $settings))) {
        $bg = $this->getColor($rgb, $settings) ?: '';
        $bg = $this->getColor($rgba, $settings) ?: $bg;
        $text = $this->getColor($text, $settings) ?: '';

        $bg = $bg ? 'background-color:' . $bg . ';' : '';
        $text = $text ? 'color:' . $text . ';' : '';

        if ($data['overlay']) {
          if ($text && $with_text) {
            $build[$key] = $text;
          }
          if ($bg) {
            $build[$bg_key] = $bg;
          }
        }
        else {
          if ($bg || $text) {
            $build[$key] = $with_text ? $bg . $text : $bg;
          }
        }
      }

      $build = array_unique($build);
    }
    return $build;
  }

}
