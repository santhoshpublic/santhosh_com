<?php

namespace Drupal\gridstack\Plugin\gridstack\stylizer;

use Drupal\Component\Serialization\Json;
use Drupal\gridstack\GridStackDefault;
use Drupal\media\Entity\Media as MediaEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the media styles for front-end.
 *
 * @GridStackStylizer(
 *   id = "media",
 *   label = @Translation("Media")
 * )
 */
class Media extends Background {

  /**
   * The field name to store media.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    if (!empty($configuration['field_name'])) {
      $instance->setFieldName($configuration['field_name']);
    }
    return $instance;
  }

  /**
   * Returns the selected field name.
   */
  public function getFieldName() {
    return $this->fieldName;
  }

  /**
   * Sets the field name.
   */
  public function setFieldName($name): self {
    $this->fieldName = $name;
    return $this;
  }

  /**
   * Returns the formatted media as Blazy CSS background.
   */
  public function buildMedia(array &$attributes, array &$settings): array {
    $data   = $this->prepareMedia($settings);
    $config = $data['#settings'];

    if ($mid = ($config['mid'] ?? NULL)) {
      $media = MediaEntity::load($mid);

      // Pass results to \Drupal\blazy\BlazyEntity.
      $data['#entity']  = $media;
      $data['#delta']   = $settings['delta'] ?? 0;
      $data['fallback'] = $media->label();

      $attributes['class'][] = 'is-gs-media';

      return $this->blazyEntity->build($data);
    }
    return [];
  }

  /**
   * Returns the formatted media as Blazy output.
   */
  protected function prepareMedia(array $settings) {
    $build = [];
    $data = $this->mediaSettings($settings);
    $blazies = $settings['blazies'] ?? NULL;

    if (!empty($data['mid'])) {
      // Unlike Bootstrap, Foundation wants large-12, etc., not cell-12.
      // Specific for this, Foundation is less efficient than Bootstrap.
      $build['#wrapper_attributes']['class'][] = 'box__bg';
      $build['#attributes']['class'][] = 'b-noratio b-gs';
      $build['#attributes']['class'][] = $this->getStyle('background', $settings) ? 'b-gs--bg' : 'b-gs--media';

      if ($animations = $this->getAnimation($settings, 'all')) {
        foreach ($animations as $key => $value) {
          $key = str_replace('_', '-', $key);
          $key = $key == 'animation' ? $key : 'animation-' . $key;

          // @todo post blazy 2.1:
          if ($key == 'animation') {
            $data['fx'] = $value;
          }
          else {
            $build['#attributes']['data-' . $key] = $value;
          }
        }
        // @todo $build['media_attributes']['class'][] = 'box__animated';
      }

      // Overlay is only applicable to image, not video, of course.
      $source = $data['media_source'] ?? '';
      $source = $blazies ? $blazies->get('media.source', $source) : $source;
      if ($source == 'image') {
        $this->prepareOverlay($build, $settings);
      }

      // Image opacity.
      if (!empty($data['opacity']) && $data['opacity'] < 1) {
        $build['#attributes']['style'] = 'opacity: ' . $data['opacity'] . ';';
      }

      // Disable any effect, we'll toggle by JS till required instead.
      // @todo figure out to make multiple animations work.
      // @todo if ($gridstacks->is('ipe')) {
      // @todo $data['_fx'] = '';
      // @todo }
    }

    $blazies->set('was.initialized', FALSE);
    $build['#settings'] = $data;
    $build['#settings']['blazies'] = $blazies;
    return $build;
  }

  /**
   * Returns the media overlay.
   */
  protected function prepareOverlay(array &$build, array $settings) {
    $gridstacks = $settings['gridstacks'];
    $bg = $this->getBackgroundColor($settings, FALSE);
    $use_overlay = $this->getStyle('overlay', $settings);

    // Always output for admin for live preview to work.
    // @todo use ::pseudo selector for front-end.
    $overlay = [
      '#theme' => 'container',
      '#attributes' => ['class' => ['media__overlay']],
    ];

    // Only inline CSS at admin pages, front-end should be cleaner.
    if ($use_overlay && $bg && $gridstacks->is('ipe')) {
      $value = is_array($bg) ? reset($bg) : $bg;
      $overlay['#attributes']['style'] = $value;
    }

    $build['overlay']['gridstack_overlay'] = $overlay;
    $build['overlay']['gridstack_overlay']['#weight'] = 100;

    // Remove from front-end if not configured so.
    if (!$gridstacks->is('ipe')) {
      if ($use_overlay && empty($bg)) {
        unset($build['overlay']['gridstack_overlay']);
      }
    }
  }

  /**
   * Returns the data understood by Blazy for CSS background.
   */
  protected function mediaSettings(array $settings) {
    $styles = empty($settings['styles']) ? [] : $settings['styles'];
    $data   = empty($styles['metadata']) ? [] : Json::decode($styles['metadata']);

    // Hard-code settings for now to limit too many possibilities.
    if ($data = array_filter($data)) {
      $blazies = $settings['blazies'];

      if ($uri = $data['uri'] ?? NULL) {
        $blazies->set('image.uri', $uri);
        unset($data['uri']);
      }

      if ($image_url = $data['image_url'] ?? NULL) {
        $blazies->set('image.url', $image_url);
        unset($data['image_url']);
      }

      $data['background']   = $this->getStyle('background', $settings);
      $data['media_switch'] = 'media';
      $data['ratio']        = '';
      $data['_detached']    = FALSE;

      $data = array_merge($styles, $data);
      return $data + GridStackDefault::entitySettings();
    }
    return [];
  }

}
