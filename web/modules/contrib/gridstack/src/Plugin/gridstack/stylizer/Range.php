<?php

namespace Drupal\gridstack\Plugin\gridstack\stylizer;

use Drupal\gridstack\GridStackDefault;

/**
 * Provides the range styles.
 *
 * @GridStackStylizer(
 *   id = "range",
 *   label = @Translation("Range")
 * )
 */
class Range extends Animation {

  /**
   * Returns the range element.
   */
  protected function rangeElement(array $settings) {
    $element = [];

    foreach (GridStackDefault::rangeElements() as $key) {
      $default = 1;
      $min = 0;
      $max = 1;
      $step = 0.05;

      if (strpos($key, 'height') !== FALSE) {
        $default = 0;
        $min = 0;
        $max = 6000;
        $step = 1;
      }

      $value = empty($settings[$key]) ? $default : $settings[$key];

      $element[$key] = $this->styleElement($key, $value, $settings);
      $element[$key]['#min'] = $min;
      $element[$key]['#max'] = $max;
      $element[$key]['#step'] = $step;
      $element[$key]['#field_prefix'] = $value;
    }

    return $element;
  }

}
