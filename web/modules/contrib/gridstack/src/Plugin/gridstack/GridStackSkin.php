<?php

namespace Drupal\gridstack\Plugin\gridstack;

use Drupal\gridstack\Skin\GridStackSkinPluginBase;

/**
 * Provides gridstack skins.
 *
 * @GridStackSkin(
 *   id = "gridstack_skin",
 *   label = @Translation("GridStack skin")
 * )
 */
class GridStackSkin extends GridStackSkinPluginBase {

  /**
   * Sets the gridstack skins.
   *
   * @inheritdoc
   */
  protected function setSkins() {
    // If you copy this file, be sure to add base_path() before any asset path
    // (css or js) as otherwise failing to load the assets. Your module can
    // register paths pointing to a theme.
    return [
      'default' => [
        'name' => 'Default',
        'provider' => 'gridstack',
        'css' => [
          'theme' => [
            'css/theme/gridstack.theme--default.css' => [],
          ],
        ],
      ],
      'selena' => [
        'name' => 'Selena',
        'provider' => 'gridstack',
        'css' => [
          'theme' => [
            'css/theme/gridstack.theme--selena.css' => [],
          ],
        ],
      ],
    ];
  }

}
