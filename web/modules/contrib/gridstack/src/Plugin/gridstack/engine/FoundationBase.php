<?php

namespace Drupal\gridstack\Plugin\gridstack\engine;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\gridstack\GridStackDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the base Foundation layout engine.
 */
abstract class FoundationBase extends GridBase {

  /**
   * {@inheritdoc}
   */
  protected $colClass = 'cell';

  /**
   * {@inheritdoc}
   */
  protected $colPrefix = 'large-';

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $sizes = $instance->sizes ?: GridStackDefault::breakpoints();
    array_pop($sizes);
    array_shift($sizes);
    $instance->sizes = $instance->setSizes($sizes);
    return $instance;
  }

  /**
   * Sets the gridstack engine classes.
   *
   * @inheritdoc
   */
  protected function setClassOptions() {
    if (!isset($this->setClassOptions)) {
      $text = $text_align = $text_transform = [];
      $utility = ['clearfix', 'radius', 'rounded', 'shadow'];
      $visibility = [
        'hide',
        'visible',
        'show-for-medium',
        'show-for-large',
        'show-for-small-only',
        'show-for-medium-only',
        'show-for-large-only',
        'hide-for-medium',
        'hide-for-large',
        'hide-for-small-only',
        'hide-for-medium-only',
        'hide-for-large-only',
      ];

      foreach (['hide', 'nowrap', 'truncate', 'wrap'] as $key) {
        $text[] = 'text-' . $key;
      }
      foreach (['left', 'right', 'center', 'justify'] as $key) {
        $text_align[] = 'text-' . $key;
      }
      foreach (['lowercase', 'uppercase', 'capitalize'] as $key) {
        $text_transform[] = 'text-' . $key;
      }

      // Classes, keyed by group.
      $this->setClassOptions = NestedArray::mergeDeep([
        'text' => $text,
        'text_align' => $text_align,
        'text_transform' => $text_transform,
        'utility' => $utility,
        'visibility' => $visibility,
      ], $this->getVersionClasses());
    }

    return $this->setClassOptions;
  }

  /**
   * {@inheritdoc}
   */
  protected function itemAttributes(array &$attributes, array &$settings) {
    parent::itemAttributes($attributes, $settings);

    $config  = $settings['gridstacks'];
    $classes = $this->optimizeClasses($config);
    $column  = $classes['column'];
    $region  = $classes['region'];

    if (!empty($region[0])) {
      $attributes['class'][] = 'box--' . Html::cleanCssIdentifier($region[0]);
    }

    if ($config->is('ungrid')) {
      return;
    }

    foreach ($this->sizes as $point => $label) {
      if (!isset($column[$point])) {
        continue;
      }

      if ($column[$point]) {
        $attributes['class'][] = $this->colClass . ' ' . $label . '-' . $column[$point];
      }
    }
  }

}
