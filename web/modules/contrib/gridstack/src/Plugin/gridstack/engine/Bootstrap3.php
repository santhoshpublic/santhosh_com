<?php

namespace Drupal\gridstack\Plugin\gridstack\engine;

use Drupal\gridstack\GridStackDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a GridStack Bootstrap 3 layout engine.
 *
 * @GridStackEngine(
 *   id = "bootstrap3",
 *   group = "bootstrap",
 *   hidden = "false",
 *   version = "3",
 *   label = @Translation("Bootstrap 3")
 * )
 */
class Bootstrap3 extends BootstrapBase {

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $sizes = $instance->sizes ?: GridStackDefault::breakpoints();
    array_pop($sizes);
    $instance->sizes = $instance->setSizes($sizes);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getVersionClasses() {
    $classes = parent::getVersionClasses();
    $hidden = $visible = [];
    foreach (array_keys($this->sizes) as $size) {
      $hidden[] = "hidden-$size";
      $visible[] = "visible-$size";
    }

    $classes['hidden'] = $hidden;
    $classes['visible'] = $visible;

    return $classes;
  }

}
