<?php

namespace Drupal\gridstack\Plugin\gridstack\engine;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;

/**
 * Provides the base Bootstrap layout engine.
 */
abstract class BootstrapBase extends GridBase {

  /**
   * {@inheritdoc}
   */
  protected $containerClasses = ['row'];

  /**
   * {@inheritdoc}
   */
  protected $nestedContainerClasses = ['row'];

  /**
   * Sets the gridstack engine classes.
   *
   * @inheritdoc
   */
  protected function setClassOptions() {
    if (!isset($this->setClassOptions)) {
      $bg = $utility = $text_color = $text_align = $text_transform = [];

      // Utility.
      $utility[] = 'clearfix';

      // Text align.
      foreach (['left', 'right', 'center', 'justify', 'nowrap', 'truncate'] as $key) {
        $text_align[] = 'text-' . $key;
      }

      // Text transform.
      foreach (['lowercase', 'uppercase', 'capitalize'] as $key) {
        $text_transform[] = 'text-' . $key;
      }

      // Background/ text color classes.
      foreach ($this->colors() as $type) {
        $bg[] = "bg-$type";
        $text_color[] = "text-$type";
      }

      $text_color[] = 'text-muted';

      $font_size = [
        'h1',
        'h2',
        'h3',
        'h4',
        'h5',
        'h6',
        'lead',
        'display-1',
        'display-2',
        'display-3',
        'display-4',
        'small',
      ];

      $height = [
        'min-vh-100',
        'vh-100',
      ];

      foreach ([25, 50, 75, 100, 'auto'] as $key) {
        $height[] = 'h-' . $key;
      }

      $padding = [];
      $prefixes = ['p', 'py', 'px'];
      foreach ($prefixes as $prefix) {
        foreach (range(0, 5) as $key) {
          $padding[] = $prefix . '-' . $key;
        }
      }

      $margin = [];
      $prefixes = ['m', 'my', 'mx'];
      foreach ($prefixes as $prefix) {
        foreach (range(0, 5) as $key) {
          $margin[] = $prefix . '-' . $key;
        }
      }

      // Classes, keyed by group.
      $this->setClassOptions = NestedArray::mergeDeep([
        'background' => $bg,
        'font_size' => $font_size,
        'height' => $height,
        'margin' => $margin,
        'padding' => $padding,
        'text_align' => $text_align,
        'text_color' => $text_color,
        'text_transform' => $text_transform,
        'utility' => $utility,
      ], $this->getVersionClasses());
    }
    return $this->setClassOptions;
  }

  /**
   * Returns the base colors.
   */
  protected function baseColors() {
    return ['primary', 'danger', 'info', 'warning', 'success'];
  }

  /**
   * Returns the colors.
   */
  protected function colors() {
    return $this->baseColors();
  }

  /**
   * {@inheritdoc}
   */
  protected function itemAttributes(array &$attributes, array &$settings) {
    parent::itemAttributes($attributes, $settings);

    $config   = $settings['gridstacks'];
    $classes  = $this->optimizeClasses($config);
    $column   = $classes['column'];
    $region   = $classes['region'];
    $colclass = (string) $this->colClass;

    if (!empty($region[0])) {
      $attributes['class'][] = 'box--' . Html::cleanCssIdentifier($region[0]);
    }

    // Allows custom overrides such as for parallax layouts.
    if ($config->is('ungrid')) {
      return;
    }

    // Bootstrap 4 uses flexbox with `col` class, and has `xl` breakpoint.
    $attributes['class'][] = $colclass;

    foreach (array_keys($this->sizes) as $point) {
      if (!isset($column[$point])) {
        continue;
      }

      // Specific to XS: Bootstrap 3: col-xs-*, Bootstrap 4: col-*.
      $col_classes = $colclass . '-' . $point . '-';
      if ($config->get('ui.framework') != 'bootstrap3' && $point == 'xs') {
        $col_classes = $colclass . '-';
      }

      if ($column[$point]) {
        $attributes['class'][] = $col_classes . $column[$point];
      }
    }
  }

}
