<?php

namespace Drupal\gridstack\Plugin\Field\FieldFormatter;

use Drupal\blazy\Plugin\Field\FieldFormatter\BlazyMediaFormatterBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\gridstack\GridStackDefault;

/**
 * Plugin implementation of the 'GridStack Media' formatter.
 *
 * @FieldFormatter(
 *   id = "gridstack_media",
 *   label = @Translation("GridStack Media"),
 *   description = @Translation("Display the core Media as a GridStack."),
 *   field_types = {"entity_reference"},
 *   quickedit = {"editor" = "disabled"}
 * )
 */
class GridStackMediaFormatter extends BlazyMediaFormatterBase {

  use GridStackFormatterTrait;

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'gridstack';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $captionId = 'caption';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return GridStackDefault::extendedSettings() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginScopes(): array {
    return [
      'vanilla'          => TRUE,
      'no_ratio'         => TRUE,
      'responsive_image' => TRUE,
    ] + parent::getPluginScopes();
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $storage = $field_definition->getFieldStorageDefinition();

    return $storage->isMultiple() && $storage->getSetting('target_type') === 'media';
  }

}
