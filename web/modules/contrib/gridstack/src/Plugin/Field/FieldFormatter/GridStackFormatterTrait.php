<?php

namespace Drupal\gridstack\Plugin\Field\FieldFormatter;

use Drupal\blazy\Plugin\Field\FieldFormatter\BlazyFormatterTrait;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Trait common for gridstack formatters.
 */
trait GridStackFormatterTrait {

  use BlazyFormatterTrait {
    injectServices as blazyInjectServices;
    getCommonFieldDefinition as blazyCommonFieldDefinition;
  }

  /**
   * Overrides the blazy manager.
   */
  public function blazyManager() {
    return $this->formatter;
  }

  /**
   * Returns the gridstack admin service shortcut.
   */
  public function admin() {
    return \Drupal::service('gridstack.admin');
  }

  /**
   * Injects DI services.
   */
  protected static function injectServices($instance, ContainerInterface $container, $type = '') {
    $instance = static::blazyInjectServices($instance, $container, $type);
    $instance->formatter = $instance->blazyManager = $container->get('gridstack.formatter');
    $instance->manager = $container->get('gridstack.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getFieldStorageDefinition()->isMultiple();
  }

  /**
   * {@inheritdoc}
   */
  protected function pluginSettings(&$blazies, array &$settings): void {
    $blazies->set('is.blazy', TRUE)
      // @todo remove post blazy:2.18.
      ->set('item.id', 'box')
      ->set('namespace', 'gridstack');
  }

  /**
   * {@inheritdoc}
   *
   * @todo remove post blazy:2.18.
   */
  public function getCommonFieldDefinition() {
    return [
      'namespace' => 'gridstack',
      'style'     => FALSE,
      'grid_form' => FALSE,
    ] + $this->blazyCommonFieldDefinition();
  }

}
