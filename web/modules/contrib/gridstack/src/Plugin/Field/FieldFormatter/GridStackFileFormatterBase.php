<?php

namespace Drupal\gridstack\Plugin\Field\FieldFormatter;

use Drupal\blazy\Plugin\Field\FieldFormatter\BlazyFormatterBlazy;
use Drupal\gridstack\GridStackDefault;

/**
 * Base class for gridstack image and file ER formatters.
 */
abstract class GridStackFileFormatterBase extends BlazyFormatterBlazy {

  use GridStackFormatterTrait;

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'gridstack';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $captionId = 'caption';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return GridStackDefault::imageSettings() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginScopes(): array {
    return [
      'background'       => TRUE,
      'no_ratio'         => TRUE,
      'responsive_image' => TRUE,
    ] + parent::getPluginScopes();
  }

}
