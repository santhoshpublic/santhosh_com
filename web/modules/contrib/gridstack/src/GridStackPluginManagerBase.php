<?php

namespace Drupal\gridstack;

use Drupal\Component\Plugin\Mapper\MapperInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Implements GridStackPluginManagerInterface.
 */
abstract class GridStackPluginManagerBase extends DefaultPluginManager implements GridStackPluginManagerInterface, MapperInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The plugin path.
   *
   * @var string
   */
  protected static $path = 'Plugin/gridstack';

  /**
   * The plugin interface.
   *
   * @var string
   */
  protected static $interface = 'Drupal\gridstack\Skin\GridStackSkinPluginInterface';

  /**
   * The plugin annotation.
   *
   * @var string
   */
  protected static $annotation = 'Drupal\gridstack\Annotation\GridStackSkin';

  /**
   * The plugin key.
   *
   * @var string
   */
  protected static $key = 'gridstack_skin';

  /**
   * The plugin attachments.
   *
   * @var array
   */
  protected $attachments;

  /**
   * Checks plugin attachments.
   *
   * @var bool
   */
  protected $checkAttachments;

  /**
   * The plugin implementors.
   *
   * @var array
   */
  protected $implementors;

  /**
   * The cached data.
   *
   * @var array
   */
  protected $cachedData;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    ConfigFactoryInterface $config
  ) {
    parent::__construct(static::$path, $namespaces, $module_handler, static::$interface, static::$annotation);

    $this->config = $config;
    $this->alterInfo(static::$key . '_info');
    $this->setCacheBackend($cache_backend, static::$key . '_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getCache() {
    return $this->cacheBackend;
  }

  /**
   * Returns gridstack config shortcut.
   */
  public function config($key = '', $group = 'gridstack.settings') {
    return $this->config->get($group)->get($key);
  }

  /**
   * {@inheritdoc}
   */
  public function load($id, array $configuration = []) {
    return $id && $this->hasDefinition($id)
      ? $this->createInstance($id, $configuration) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $configuration = []) {
    $plugins = [];
    foreach ($this->getDefinitions() as $definition) {
      array_push($plugins, $this->createInstance($definition['id'], $configuration));
    }
    return $plugins;
  }

  /**
   * Collects attachments from plugins.
   */
  protected function checkAttachments(array &$load, array $attach, $config) {
    if (!$this->checkAttachments) {
      if ($implementors = $this->getImplementors('attach')) {
        foreach ($implementors as $id) {
          $this->load($id, $attach)->attach($load, $attach, $config);
        }
      }
      $this->checkAttachments = TRUE;
    }
  }

  /**
   * Returns plugins names implementing a method.
   *
   * @todo drop this approach.
   */
  protected function getImplementors($method) {
    $cid = static::$key . '_' . $method;
    if ($data = $this->getCachedData($cid)) {
      return $data;
    }

    if (!isset($this->implementors[$cid])) {
      $data = [];
      foreach ($this->loadMultiple() as $plugin) {
        $class = $plugin->get('class');
        $reflection = new \ReflectionClass($class);

        if ($reflection->getMethod($method)->class == $class) {
          $data[$plugin->getPluginId()] = $plugin->getPluginId();
        }
      }

      ksort($data);
      $this->implementors[$cid] = $data;
    }

    $data = $this->implementors[$cid] ?: [];

    return $this->getCachedData($cid, $data, TRUE);
  }

  /**
   * Returns gridstack plugin data.
   */
  protected function getData(
    array $methods,
    $flatten = FALSE,
    array $configuration = []
  ) {
    $cid = static::$key . 's_data';

    if ($data = $this->getCachedData($cid)) {
      return $data;
    }

    $data = $items = [];
    foreach ($this->loadMultiple($configuration) as $plugin) {
      if ($flatten) {
        foreach ($methods as $method) {
          $data = NestedArray::mergeDeep($data, $plugin->{$method}());
        }
      }
      else {
        foreach ($methods as $method) {
          $items[$method] = $plugin->{$method}();
        }
        $data = NestedArray::mergeDeep($data, $items);
      }
    }

    ksort($data);
    return $this->getCachedData($cid, $data, TRUE);
  }

  /**
   * Returns available data for select options.
   */
  public function getDataOptions(array $data) {
    $options = [];
    foreach ($data as $key => $properties) {
      $options[$key] = $properties['name'] ?? $key;
    }
    ksort($options);
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * @todo use Blazymanager::getCachedData() post blazy::2.10.
   */
  protected function getCachedData(
    $cid,
    array $data = [],
    $reset = FALSE,
    $alter = NULL,
    array $context = []
  ): array {
    if (!isset($this->cachedData[$cid]) || $reset) {
      $cache = $this->cacheBackend->get($cid);

      if (!$reset && $cache && $data = $cache->data) {
        $this->cachedData[$cid] = $data;
      }
      else {
        // Allows empty array to trigger hook_alter.
        if (is_array($data)) {
          $this->moduleHandler->alter($alter ?: $cid, $data, $context);
        }

        // Only if we have data, cache them.
        if ($data && is_array($data)) {
          if (isset($data[1])) {
            $data = array_unique($data);
          }

          ksort($data);

          $count = count($data);
          $tags = Cache::buildTags($cid, ['count:' . $count]);
          $this->cacheBackend->set($cid, $data, Cache::PERMANENT, $tags);
        }
        $this->cachedData[$cid] = $data;
      }
    }

    return $this->cachedData[$cid] ? array_filter($this->cachedData[$cid]) : [];
  }

}
