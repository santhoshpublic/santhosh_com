<?php

namespace Drupal\gridstack\Form;

/**
 * Provides reusable admin functions or form elements specific for stylings.
 */
interface GridStackAdminStylizerInterface extends GridStackAdminInterface {}
