<?php

namespace Drupal\gridstack\Form;

/**
 * Provides resusable admin functions or form elements.
 *
 * @todo recheck if to extend BlazyAdminInterface. The reason it never extends
 * it is to avoid blocking Blazy adjustments due to still recognizing similar
 * features across sub-modules to DRY.
 */
interface GridStackAdminInterface {

  /**
   * Returns the blazy admin formatter.
   */
  public function blazyAdmin();

  /**
   * Returns the slick manager.
   */
  public function manager();

  /**
   * Returns all settings form elements.
   *
   * Some settings might be required by Outlayer, such as style.
   */
  public function buildSettingsForm(array &$form, array $definition): void;

  /**
   * Returns the main form elements.
   */
  public function mainForm(array &$form, array $definition): void;

  /**
   * Modifies the opening form elements.
   */
  public function openingForm(array &$form, array &$definition): void;

  /**
   * {@inheritdoc}
   */
  public function closingForm(array &$form, array $definition): void;

  /**
   * Returns re-usable logic, styling and assets across fields and Views.
   */
  public function finalizeForm(array &$form, array $definition): void;

  /**
   * Returns available options for select options.
   */
  public function getOptionsetsByGroupOptions($group = ''): array;

  /**
   * Returns available skins for select options.
   */
  public function getSkinOptions(): array;

  /**
   * Returns default layout options for the core Image, or Views.
   */
  public function getLayoutOptions(): array;

  /**
   * Return the field formatter settings summary.
   */
  public function getSettingsSummary(array $definition): array;

  /**
   * Returns available fields for select options.
   */
  public function getFieldOptions(
    array $target_bundles = [],
    array $allowed_field_types = [],
    $entity_type = 'media',
    $target_type = ''
  ): array;

  /**
   * Returns Responsive image for select options.
   */
  public function getResponsiveImageOptions(): array;

}
