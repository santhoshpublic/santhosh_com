<?php

namespace Drupal\gridstack\Skin;

use Drupal\gridstack\GridStackPluginInterface;

/**
 * Provides an interface defining GridStack skins.
 */
interface GridStackSkinPluginInterface extends GridStackPluginInterface {

  /**
   * Returns the GridStack skins.
   *
   * This can be used to register skins for the GridStack. Skins will be
   * available when configuring the Optionset, Field formatter, or Views style,
   * or custom coded gridstacks.
   *
   * GridStack skins get a unique CSS class to use for styling, e.g.:
   * If your skin name is "my_module_gridstack_flip", the CSS class is:
   * gridstack--skin--my-module-gridstack-flip
   *
   * A skin can specify CSS and JS files to include when GridStack is displayed,
   * except for a thumbnail skin which accepts CSS only.
   *
   * Each skin supports a few keys:
   * - name: The human readable name of the skin.
   * - description: The description about the skin, for help and manage pages.
   * - css: An array of CSS files to attach.
   * - js: An array of JS files to attach, e.g.: image zoomer, reflection, etc.
   * - dependencies: Similar to how core library dependencies constructed.
   * - provider: A module name registering the skins.
   *
   * @return array
   *   The array of the skins.
   */
  public function skins();

}
