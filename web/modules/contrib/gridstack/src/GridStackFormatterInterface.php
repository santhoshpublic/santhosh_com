<?php

namespace Drupal\gridstack;

use Drupal\blazy\BlazyFormatterInterface;

/**
 * Defines re-usable services and functions for gridstack field plugins.
 */
interface GridStackFormatterInterface extends BlazyFormatterInterface {}
