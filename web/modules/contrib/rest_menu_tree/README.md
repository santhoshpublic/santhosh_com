# REST Menu Tree

Retrieve an entire menu tree in a single endpoint.

## Introduction

REST Menu Tree allows developers to retrieve an entire menu link tree via a
single endpoint. This makes it easier to build menus in external applications.

## Installation

Install as you would normally install a contributed Drupal module.

After enabling this module the resource must be enabled manually to make it
available. This can be achieved with the [REST UI](https://www.drupal.org/project/restui)
module. Additional permissions may need to be configured for the enabled
resource.

## Requirements

* [Menu Normalizer](https://www.drupal.org/project/menu_normalizer)

## Suggestions

* [REST UI](https://www.drupal.org/project/restui)

## Maintainers

Current maintainers:

* [Christopher C. Wells (wells)](https://www.drupal.org/u/wells)

Past maintainers:

* David Barratt ([davidwbarratt](https://www.drupal.org/u/davidwbarratt))

Current sponsors:

* [Cascade Public Media](https://www.drupal.org/cascade-public-media)

Past sponsors:

* [Golf Channel](https://www.drupal.org/node/2374873)
