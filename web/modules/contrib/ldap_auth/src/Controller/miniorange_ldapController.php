<?php

namespace Drupal\ldap_auth\Controller;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\ControllerBase;
use Drupal\ldap_auth\LDAPFlow;
use Drupal\ldap_auth\Mo_Ldap_Auth_Response;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\ldap_auth\Utilities;

/**
 *
 */
class miniorange_ldapController extends ControllerBase {

  private $base_url;
  private ImmutableConfig $config;
  private Config $config_factory;
  private $moduleList;

  /**
   *
   */
  public function __construct() {
    $this->base_url = \Drupal::request()->getSchemeAndHttpHost().\Drupal::request()->getBasePath();
    $this->config = \Drupal::config('ldap_auth.settings');
    $this->config_factory = \Drupal::configFactory()->getEditable('ldap_auth.settings');
    $this->moduleList = \Drupal::service('extension.list.module');
  }


  /**
   * @param $username
   * @return \Drupal\ldap_auth\Mo_Ldap_Auth_Response
   */
  public function search_user_attributes($username) {

    $ldap_connect = new LDAPFlow();
    $ldapconn = $ldap_connect->getConnection();
    $auth_response = new Mo_Ldap_Auth_Response();
    if ($ldapconn) {
      $ldap_bind_dn = $ldap_connect->getServiceAccountUsername();
      $ldap_bind_password = $ldap_connect->getServiceAccountPassword();
      $search_base = $ldap_connect->getSearchBase();
      $search_filter_1 = $ldap_connect->getSearchFilter();
      $search_filter = '(&(objectClass=*)(' . $search_filter_1 . '=?))';
      $filter = str_replace('?', $username, $search_filter);

      $user_search_result = NULL;
      $entry              = NULL;
      $info               = NULL;
      $bind               = @ldap_bind($ldapconn, $ldap_bind_dn, $ldap_bind_password);
      $err                = ldap_error($ldapconn);

      if (strtolower($err) != 'success') {
        $auth_response->status = FALSE;
        $auth_response->statusMessage = "LDAP_NOT_RESPONDING, $err";
        $auth_response->test_configuration_error = $err. "[". ldap_errno($ldapconn)."]";
        $auth_response->userDn = '';
        return $auth_response;
      }

      if (ldap_search($ldapconn, $search_base, $filter)) {

        $user_search_result = ldap_search($ldapconn, $search_base, $filter, ['*', '+']);

        $info = ldap_first_entry($ldapconn, $user_search_result);
        $entry = ldap_get_entries($ldapconn, $user_search_result);
        $user_attributes = [];
        $i = 0;

        if (!$info) {
          $err = ldap_error($ldapconn);
          $auth_response->status = FALSE;
          $auth_response->statusMessage = 'User with <b>' . $search_filter_1 . ' = "' . $username . '"</b> does not exist in selected search base and search filter<br>
                <ul>
                 <li>Selected search base : ' . $search_base . '</li>
                 <li>Selected search filter : ' . $search_filter_1 . '</li>
                </ul>';

          $auth_response->test_configuration_error = "User not exist in selected search base or search filter ($search_filter_1)." ;
          $auth_response->userDn = NULL;
          return $auth_response;
        }

        foreach ($entry[0] as $key => $value) {
          if (!is_int($key) && $key != 'count') {
            $user_attributes[$key] = $value[0];
          }
        }

        $user_attributes['dn'] = ldap_get_dn($ldapconn, $info);

        if (isset($entry[0]['memberof']) && is_array($entry[0]['memberof'])) {
          $user_attributes['memberof'] = [];
          foreach ($entry[0]['memberof'] as $member) {
            if ($i > 0) {
              array_push($user_attributes['memberof'], $member);
            }
            $i++;
          }
        }

        $auth_response->status = TRUE;
        $auth_response->statusMessage = "Attributes fetched Successfully.";
        $auth_response->test_configuration_error = "Attributes fetched Successfully.";
        $auth_response->userDn = ldap_get_dn($ldapconn, $info);
        $auth_response->attributeList = $user_attributes;
        return $auth_response;
      }
      else {
        $auth_response->status = FALSE;
        $auth_response->statusMessage = "Error fetching user information. <br><br> LDAP Error: <strong>".ldap_error($ldapconn)."</strong><br> Error Number: <strong>".ldap_errno($ldapconn)."</strong>";
        $auth_response->test_configuration_error = "Error fetching user info - ".ldap_error($ldapconn)." (".ldap_errno($ldapconn).")";
        $auth_response->userDn = NULL;
        return $auth_response;
      }
    }
    else {
      // Error message.
      $auth_response->status = FALSE;
      $auth_response->statusMessage = 'ERROR : Cannot connect to your LDAP Server';
      $auth_response->test_configuration_error = "Cannot connect to your LDAP Server";
      $auth_response->userDn = NULL;
      return $auth_response;
    }

  }

  /**
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function uninst_mod() {
    $this->config_factory->clear('miniorange_ldap_feedback_status')->save();
    \Drupal::service('module_installer')->uninstall(['ldap_auth']);
    $uninstall_redirect = $this->base_url . '/admin/modules';
    $response = new RedirectResponse($uninstall_redirect);
    $response->send();
    return new Response();
  }


  /**
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
   */
  public function test_configuration() {
    global $base_url;

    if ( (isset($_POST['user']) && $_POST['user'] == '')) {
      echo '<div style="color: #9f1e2b;background-color: #f0d8d8; padding:2%;margin-bottom:20px;text-align:left; border:1px solid #e8bcbc; font-size:16pt;"><b>Username or Password cannot be empty.</b></div>';
      exit;
    }

    $username = trim($_POST['user']) ?? '';

    $this->config_factory->set('mo_last_authenticated_user', $username)->save();

    $attributes = self::search_user_attributes($username);

    $module_path = $this->moduleList->getPath("ldap_auth");

    if (!$attributes->status) {
      echo '<div style="color: #961f1f; padding:2%;margin-bottom:20px;text-align:center;font-weight: bold; font-size:18pt;">TEST FAILED</div><div style="display:block;text-align:center;margin-bottom:0%;"></div>
            <div style="display:block;text-align:center;margin-bottom:4%;"><img style="width:12%;"src="' . $base_url . '/' . $module_path . '/assets/img/wrong.png"></div>';

      echo '<div style="color: #101010;background-color: #f0d8d8; padding:2%;margin-bottom:20px;text-align:left; border:1px solid #e8bcbc; width: fit-content; font-size:16pt;">' . $attributes->statusMessage . '</div>';
      $this->config_factory->set("miniorange_drupal_ldap_login_test",$attributes->test_configuration_error)->save();
      echo '<div style="margin:3%;display:block;text-align:center;"><input style="padding:1%;width:100px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;" type="button" value="Close" onClick="self.close();"/></div>';

      return new Response();
    }

    unset($attributes->attributeList['objectsid']);
    unset($attributes->attributeList['objectguid']);
    $name = $attributes->attributeList['cn'];

    $attributes_list = [];
    Utilities::show_attr($attributes->attributeList, $attributes_list);

    foreach ($attributes_list as $value) {
      $attr_list[$value['att_name']] = $value['att_value'];
    }

    $this->config_factory->set('miniorange_ldap_user_attributes', json_encode($attributes))->save();

    if (isset($_POST['pass']) && $_POST['pass'] != 'undefined') {
      $user_pass = $_POST['pass'];
      $ldap_connect = new LDAPFlow();
      $ldapconn = $ldap_connect->getConnection();

      $auth_response = $ldap_connect->ldap_login($username, $user_pass);

      // Error message.
      if (!$auth_response->status) {

        $this->config_factory->set("miniorange_drupal_ldap_login_test",$auth_response->statusMessage)->save();
        echo '<div style="color: #961f1f; padding:2%;margin-bottom:20px;text-align:center;font-weight: bold; font-size:18pt;">TEST FAILED</div><div style="display:block;text-align:center;margin-bottom:0%;"></div><div style="display:block;text-align:center;margin-bottom:4%;"><img style="width:12%;"src="' . $base_url . '/' . $module_path . '/assets/img/wrong.png"></div>';
        echo '<div style="color: #9f1e2b;background-color: #f0d8d8; padding:2%;margin-bottom:20px;text-align:left;width:fit-content; border:1px solid #e8bcbc; font-size:16pt;"><b>' . $auth_response->statusMessage . '</b>: User found in the LDAP server but the password does not match. Please try again with another password.<br></div>';
        echo '<div style="margin:3%;display:block;text-align:center;"><input
                            style="padding:1%;width:100px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;"
                            type="button" value="Close" onClick="self.close();"/></div>';
        return new Response();
      }
    }

    echo '<style>select {
  background-color: transparent;
  border: none;
  padding: 0 1em 0 0;
  margin: 0;
  width: 100%;
  font-family: inherit;
  font-size: inherit;
  cursor: inherit;
  line-height: inherit;
  z-index: 1;
}
                .select {
  display: grid;
  grid-template-areas: "select";
  align-items: center;
  position: relative;
  min-width: 15ch;
  max-width: 30ch;
  border: 1px solid;
  border-radius: 0.25em;
  padding: 0.25em 0.5em;
  font-size: 1.25rem;
  cursor: pointer;
  line-height: 1.1;
  background-color: #fff;
  background-image: linear-gradient(to top, #f9f9f9, #fff 33%);
}
                .flex-container {
  display: flex;
  align-items: stretch;
  background-color: #ffffff;
  color: #000;
  width: 100%;
  margin: 1px;
  text-align: center;
  line-height: 35px;
  font-size: 20px;
  justify-content: space-between;
}
                .message_div{color: #3c763d;background-color: #dff0d8; padding:2%;margin-bottom:20px;text-align:left; border:1px solid #AEDB9A; font-size:16pt;}
                .message_div1{color: #000000;background-color: #d2d1d1; padding:2%;margin-bottom:10px;text-align:left; border:1px solid #ffffff; font-size:12pt;}
                .title_div {color: #3c763d; padding:2%;margin-bottom:20px;text-align:center;font-weight: bold; font-size:18pt;}
                .save_and_close_button {padding:10%;width:130px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;}
              </style>';

    echo '<div style="font-family:Calibri;padding:0 3%;">';
    echo '<div class="title_div">TEST SUCCESSFUL</div><div style="display:block;text-align:center;margin-bottom:0;"></div><div style="display:block;text-align:center;margin-bottom:4%;"><img style="width:12%;" src="' . $base_url . '/' . $module_path . '/assets/img/green_check.png"></div>';
    echo '<div class="message_div">Congratulations, your test authentication is successful. Now, please select the email attribute. </div>';
    echo "<div class='message_div1'>Please select the <b>Attribute name</b> in which you are getting your user's <b>Email</b>.</div>";

    $email_attribute = $this->config->get('miniorange_ldap_email_attribute');

    echo '<div class="flex-container"><div style="max-width:70vh;font-weight: bold;">Email Attribute: </div>
             <div class="select">
             <select id="mo_ldap_email_attr">';
    foreach ($attr_list as $key => $value) {
      $selected = $key == $email_attribute ? "selected" : "";
      echo '<option value=' . $key . ' ' . $selected . '>' . $key . '</option> ';
    }
    echo '</select></div> <div style="display:block;text-align:left;"><input class ="save_and_close_button"
                            type="button" value="Save & Next" onClick="save_and_done();"/></div></div>';
    echo '<script>
                        function save_and_done(){
                          var email_attr = document.getElementById("mo_ldap_email_attr").value;
                          var baseurl = window.location.href;
                          var pos = baseurl.indexOf("testLdapConfig");
                          window.opener.location.href = baseurl.replace(baseurl.slice(pos), "mo_post_testconfig/?field_selected="+email_attr);
                          self.close();
                        }
                        </script>';

    $this->display_attributes($name, $attr_list);

    $this->config_factory->set("miniorange_drupal_ldap_login_test",'Success')->save();

    \Drupal::configFactory()->getEditable('ldap_auth.settings')->set('miniorange_ldap_user_attributes', json_encode($attributes))->save();
    return new Response();
  }

  /**
   * @return void
   */
  public function mo_post_testconfig() {
    $email_attribute = $_GET['field_selected'];
    $this->config_factory->set('miniorange_ldap_email_attribute', $email_attribute)->save();
    global $base_url;

    $response = new RedirectResponse($base_url . "/admin/config/people/ldap_auth/ldap_config");
    $response->send();
    return new Response();
  }

  /**
   *
   */
  public function display_attributes($name, $attr_list) {
    echo '<p style="font-size:13pt;margin-left:1%;"> Hello <b>' . $name . ',</b></p>
            <table style="border-collapse:collapse;border-spacing:0; display:table;width:100%; font-size:13pt;background-color:#ffffff;">';
    foreach ($attr_list as $key => $value) {

      if($key == "jpegphoto" || $key == "thumbnailphoto"){
        $value = "<img src='data:image/jpeg;base64,".base64_encode($value)."' alt='User photo' / width='100' height='120'>";
      }

      echo ' <tr style="text-align:left;">
                       <td style="font-weight:bold;border:2px solid #949090;padding:2%;"><b>' . $key . '</b></td>
                       <td style="padding:2%;border:2px solid #949090; word-wrap:break-word;">' . $value . '</td>
                </tr>';
    }
    echo '</table><br><br>';
  }

}
