<?php

namespace Drupal\ldap_auth;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ldap_auth\Form\LDAPFormBase;

class SearchBaseAndFilterFormBuilder extends LDAPFormBase{

  public static function insertForm(array &$form, FormStateInterface $form_state, $config,$possible_search_bases_in_key_val){
    global $base_url;
    $form['miniorange_search_base_options'] = [
        '#type' => 'value',
        '#id' => 'miniorange_search_base_options',
        '#value' => $possible_search_bases_in_key_val,
    ];
    $form['miniorange_search_base_options']['search_base_attribute'] = [
        '#id' => 'miniorange_ldap_search_base_attribute',
        '#title' => t('Search Base(s):'),
        '#type' => 'select',
        '#description' => t('Search Base indicates the location in your LDAP server where your users reside. Select the Distinguished Name(DN) of the Search Base object from the above dropdown.<br>Multiple Search Bases are supported in the <a href="' . $base_url . '/admin/config/people/ldap_auth/Licensing"><strong>[Premium, All-inclusive]</strong></a> version of the module.'),
        '#default_value' => $config->get('miniorange_ldap_search_base'),
        '#options' => $form['miniorange_search_base_options']['#value'],
        '#attributes' => ['style' => 'width:65%;height:30px'],
    ];
    $form['miniorange_ldap_custom_sb_attribute'] = [
        '#type' => 'textfield',
        '#title' => t('Other Search Base(s):'),
        '#default_value' => empty($config->get('miniorange_ldap_custom_sb_attribute')) ? reset($possible_search_bases_in_key_val) : $config->get('miniorange_ldap_custom_sb_attribute'),
        '#states' => ['visible' => [':input[name = "search_base_attribute"]' => ['value' => 'custom_base']]],
        '#attributes' => ['style' => 'width:65%;'],
    ];
    $form['miniorange_username_options'] = [
        '#type' => 'value',
        '#id' => 'miniorange_username_options',
        '#value' => [
            'samaccountName' => t('samaccountName'),
            'mail' => t('mail'),
            'userPrincipalName' => t('userPrincipalName'),
            'cn' => t('cn'),
            'sn' => t('sn'),
            'givenname' => t('givenname'),
            'uid' => t('uid'),
            'displayname' => t('displayname'),
            'custom' => t('Other'),
        ],
    ];
    $form['ldap_auth']['settings']['username_attribute'] = [
        '#id' => 'miniorange_ldap_username_attribute',
        '#title' => t('Search Filter/Username Attribute:'),
        '#type' => 'select',
        '#description' => t('Select the LDAP attribute against which the user will be searched.<br> <b>For example:</b> If you want the user to login to Drupal using their email address( the one present in the LDAP server), you can select <b>mail</b> in the dropdown.<br>You can even search for your user using a Custom Search Filter in the <a href="' . $base_url . '/admin/config/people/ldap_auth/Licensing"><strong>[Premium, All-inclusive]</strong></a> version of the module<div><br>'
        ),
        '#default_value' => $config->get('miniorange_ldap_username_attribute_option'),
        '#options' => $form['miniorange_username_options']['#value'],
        '#attributes' => ['style' => 'width:65%;height:30px'],
    ];
    $form['miniorange_ldap_custom_username_attribute'] = [
        '#type' => 'textfield',
        '#title' => t('Other Attribute'),
        '#default_value' => $config->get('miniorange_ldap_custom_username_attribute'),
        '#states' => ['visible' => [':input[name = "username_attribute"]' => ['value' => 'custom']]],
        '#attributes' => ['style' => 'width:65%;'],
    ];

    $form['back_step_3'] = [
        '#type' => 'submit',
        '#button_type' => 'danger',
        '#prefix' => "<div class='pito_enable_alignment'>",
        '#value' => t('&#171; Back'),
        '#submit' => ['::miniorange_ldap_back_3'],
        '#attributes' => ['style' => 'display: inline-block;'],
    ];
    $form['next_step_3'] = [
        '#type' => 'submit',
        '#button_type' => 'primary',
        '#value' => t('Next &#187; '),
        '#suffix' => "</div></div>",
        '#attributes' => ['style' => 'float: right;display:block;'],
        '#submit' => ['::miniorange_ldap_next3'],
    ];
    return $form;
  }
}
