# Releasing steps

1. Check CI status: https://www.drupal.org/pift-ci-job/2744796
2. `yarn changelog` to output changelog entries for unreleased commits to
   stdout, these can be pasted into the release node for drupal.org in step 4.
3. `yarn bump [version-string]` (for pre-stable only, once we hit 1.0.0 we can
   use `yarn stable-bump` and probably rename that script to simply `bump`)
4. `git push --tags`
5. Create release node: https://www.drupal.org/node/add/project-release/3349132
