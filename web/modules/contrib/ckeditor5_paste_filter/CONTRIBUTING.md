# Contribution guidelines

We welcome contributions of all types and sizes! This documentation may look a
bit scary but we don't expect a casual contributor to follow all of these
guidelines or tick all of these boxes. For example, maintainers and other
contributors will help write tests if a bugfix is missing test coverage, and
will rewrite commit messages to follow conventions.

This documentation serves the maintainers as well and outlines what is expected
of current and future maintainers.

See also [RELEASING.md](RELEASING.md).

## Coding standards

We follow the [Drupal coding standards](https://www.drupal.org/docs/develop/standards).

## Compiling JavaScript

From the project root:

```
yarn install # Only required the first time, or if dependencies change.
yarn build
```

There is also `yarn watch`, `yarn lint`, `yarn fix`.

## Test coverage

All bug fixes and new features must have test coverage, we have a suite of
Nightwatch tests that cover the functionality of the module.

## Committing

As much as possible, the maintainers aim to make focused, atomic commits that
can stand on their own and can fit into one of the types defined below.

Specifically, we follow the [conventional commits specification]. Our commit
messages are then used to generate release notes. See
[package.json](package.json) for how this is set up.

[conventional commits specification]: https://www.conventionalcommits.org/en/v1.0.0/

### Commit types

- `chore`: Updates to internal tooling, release-related tasks, etc.
- `ci`: CI updates (GitLab CI or Drupal CI)
- `docs`: Documentation only changes
- `feat`: A new feature
- `fix`: A bug fix
- `perf`: A code change that improves performance
- `refactor`: A code change that neither fixes a bug nor adds a feature, and
  specifically does not change any functionality
- `style`: Changes that do not affect the meaning of the code (white-space,
  formatting, missing semi-colons, etc)
- `test`: Testing updates

### Breaking changes

If a type is followed by `!`, that signifies a breaking change.

### Referencing Drupal.org issues

To reference a Drupal.org issue number, add it as a trailer to the git commit
message as shown below. This will ensure that the commit on GitLab links to the
Drupal.org issue and vice versa.

Currently, we only use `Fixes` and `References` as trailers.

```
feat: turbo encabulator

Fixes: #12345678
```
