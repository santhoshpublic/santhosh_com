# mosparo Integration

This module integrates mosparo into your forms. You can use it in the Contact
forms, the Webform forms, or the CAPTCHA module.

The Contact and Webform modules add a new field type which you can add to your
forms in the user interface. When you use the CAPTCHA module, you have to
enable the mosparo integration on the forms you want to have mosparo enabled.

## Requirements

To use the plugin, you must meet the following requirements:

### mosparo: Base Integration

The base module, which is required to be enabled for all of the three
submodules, has the following requirements:

- Drupal 9.4.x+/10.x+
- A mosparo installation with a project

### mosparo: Integration for CAPTCHA

- [CAPTCHA](https://www.drupal.org/project/captcha) 1.9+

### mosparo: Integration for Contact

Since the Contact module is a core module, there are no special requirements
for this submodule.

### mosparo: Integration for Webform

- [Webform](https://www.drupal.org/project/webform) 6.1+

## Installation

Install the module as you install every other Drupal module
(see [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)).

## Usage

After you've installed the module, execute the following steps:

1. Enable the base module and the submodules in the Administration
(Administration > Extend).
2. Navigate to Administration > Configuration > mosparo Connections to
administer the connection to your mosparo installation.
3. Add at least one connection. You can add multiple connections to use
different connections in different forms.
4. Enable or add mosparo in the interface of the submodule you've chosen
(for example: go to Administration > Configuration > People > Captcha.
Enable the Captcha functionality on the form you want and choose the mosparo
connection as Captcha method).
