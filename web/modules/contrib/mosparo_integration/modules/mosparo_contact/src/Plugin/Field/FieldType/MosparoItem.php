<?php

namespace Drupal\mosparo_contact\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the mosparo field type.
 *
 * @FieldType(
 *   id = "mosparo_contact",
 *   label = @Translation("mosparo"),
 *   description = @Translation("A field to protect a form with a mosparo verification."),
 *   category = @Translation("General"),
 *   default_widget = "mosparo_default",
 *   default_formatter = "boolean"
 * )
 */
class MosparoItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'mosparo_connection' => new TranslatableMarkup('mosparo connection'),
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['mosparo_connection'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('mosparo Connection'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $options = [];

    $mosparo_connection_storage = \Drupal::entityTypeManager()->getStorage('mosparo_connection');
    $ids = \Drupal::entityQuery('mosparo_connection')->execute();
    $connections = $mosparo_connection_storage->loadMultiple($ids);
    foreach ($connections as $connection) {
      $options[$connection->getId()] = $connection->getLabel();
    }

    $element['mosparo_connection'] = [
      '#type' => 'select',
      '#title' => $this->t('mosparo Connection'),
      '#default_value' => $this->getSetting('mosparo_connection'),
      '#options' => $options,
    ];

    return $element;
  }

}
