<?php

namespace Drupal\mosparo_contact\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'mosparo_default' widget.
 *
 * @FieldWidget(
 *   id = "mosparo_default",
 *   label = @Translation("mosparo"),
 *   field_types = {
 *     "mosparo_contact"
 *   }
 * )
 */
class MosparoWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $connection = \Drupal::entityTypeManager()->getStorage('mosparo_connection')->load($this->getFieldSetting('mosparo_connection'));
    $mosparoHelper = \Drupal::service('mosparo_integration.service');

    $designMode = FALSE;
    $buildInfo = $form_state->getBuildInfo();
    if (isset($buildInfo['base_form_id']) && $buildInfo['base_form_id'] === 'field_config_form') {
      $designMode = TRUE;
    }

    $fieldData = [];
    if ($connection !== NULL) {
      $attached = $mosparoHelper->generateAttached($connection);

      $fieldData = [
        '#markup' => $mosparoHelper->generateHtml($connection, $designMode),
        '#attached' => $attached,
      ];
    }

    $form['#validate'][] = function ($form, $form_state) use ($connection) {
      mosparo_contact_form_validate($form, $form_state, $connection, $this);
    };

    $element['value'] = $element + $fieldData;

    return $element;
  }

}
