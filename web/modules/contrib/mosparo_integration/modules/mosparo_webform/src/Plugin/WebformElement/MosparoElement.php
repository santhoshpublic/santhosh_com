<?php

namespace Drupal\mosparo_webform\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\WebformSubmissionInterface;
use Mosparo\ApiClient\Exception;

/**
 * Provides an webform element to add mosparo to a webform form.
 *
 * @WebformElement(
 *   id = "mosparo_webform",
 *   default_key = "mosparo",
 *   label = @Translation("mosparo"),
 *   category = @Translation("Advanced elements"),
 *   description = @Translation("Provides an element to protect your form with mosparo."),
 * )
 */
class MosparoElement extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
      'mosparo_connection' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#input' => TRUE,
      '#size' => 60,
      '#required' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['mosparo'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('mosparo Settings'),
    ];

    $options = [];
    $mosparo_connection_storage = \Drupal::entityTypeManager()->getStorage('mosparo_connection');
    $ids = \Drupal::entityQuery('mosparo_connection')->execute();
    $connections = $mosparo_connection_storage->loadMultiple($ids);
    foreach ($connections as $connection) {
      $options[$connection->getId()] = $connection->getLabel();
    }

    $form['mosparo']['mosparo_connection'] = [
      '#type' => 'select',
      '#title' => $this->t('mosparo Connection'),
      '#options' => $options,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);

    $connection = \Drupal::entityTypeManager()->getStorage('mosparo_connection')->load($element['#mosparo_connection']);
    $mosparoHelper = \Drupal::service('mosparo_integration.service');

    $fieldData = [];
    if ($connection !== NULL) {
      $attached = $mosparoHelper->generateAttached($connection);

      $fieldData = [
        '#markup' => $mosparoHelper->generateHtml($connection),
        '#attached' => $attached,
      ];
    }

    $element['#element_validate'][] = [$this, 'validateMosparoForm'];

    $element['value'] = $fieldData;
  }

  /**
   * Verifies the submitted form data with the help of mosparo.
   *
   * @param array $element
   *   The mosparo element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state of the form.
   * @param array $complete_form
   *   The complete form as array.
   */
  public static function validateMosparoForm(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $connection = \Drupal::entityTypeManager()->getStorage('mosparo_connection')->load($element['#mosparo_connection']);
    $mosparoHelper = \Drupal::service('mosparo_integration.service');

    $fieldInformation = $mosparoHelper->extractFieldInformation($form_state);

    [
      $formData,
      $requiredFields,
      $verifiableFields,
      $submitToken,
      $validationToken,
    ] = $mosparoHelper->prepareFormData(
      $form_state->getUserInput(),
      $fieldInformation,
      ['captcha', 'captcha_sid', 'captcha_token', 'captcha_cacheable']
    );

    $client = $mosparoHelper->getApiClient($connection);

    try {
      /** @var \Mosparo\ApiClient\VerificationResult $res */
      $res = $client->verifySubmission($formData, $submitToken, $validationToken);

      if ($res !== NULL) {
        $verifiedFields = array_keys($res->getVerifiedFields());
        $requiredFieldDifference = array_diff($requiredFields, $verifiedFields);
        $verifiableFieldDifference = array_diff($verifiableFields, $verifiedFields);

        if ($res->isSubmittable() && empty($requiredFieldDifference) && empty($verifiableFieldDifference)) {
          return;
        }
        else {
          $form_state->setError($element, t('The form is not submittable.'));
        }
      }
      else {
        foreach ($res->getIssues() as $issue) {
          $form_state->setError($element, t('mosparo returned an error. Error: :message', [':message' => $issue['message']]));
        }
      }
    }
    catch (Exception $e) {
      $form_state->setError($element, $e->getMessage());
    }
  }

}
