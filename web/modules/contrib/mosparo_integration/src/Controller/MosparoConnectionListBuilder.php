<?php

namespace Drupal\mosparo_integration\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * List builder for the mosparo connection entity list.
 *
 * @package Drupal\mosparo_integration
 */
class MosparoConnectionListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('label');
    $header['mosparoHost'] = $this->t('mosparo host');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->getLabel();
    $row['mosparoHost'] = $entity->getMosparoHost();

    return $row + parent::buildRow($entity);
  }

}
