<?php

namespace Drupal\mosparo_integration\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity Form to edit mosparo connection.
 *
 * @package Drupal\mosparo_integration
 */
class MosparoConnectionForm extends EntityForm {

  /**
   * Constructs the form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\mosparo_integration\MosparoConnectionInterface $mosparo_connection */
    $mosparo_connection = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('Also works with the base form ID.'),
      '#default_value' => $mosparo_connection->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $mosparo_connection->getId(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$mosparo_connection->isNew(),
    ];

    $form['mosparoHost'] = [
      '#type' => 'textfield',
      '#title' => $this->t('mosparo Host'),
      '#description' => '',
      '#default_value' => $mosparo_connection->getMosparoHost(),
      '#required' => TRUE,
    ];

    $form['mosparoUuid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('mosparo UUID'),
      '#description' => '',
      '#default_value' => $mosparo_connection->getMosparoUuid(),
      '#required' => TRUE,
    ];

    $form['mosparoPublicKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('mosparo Public key'),
      '#description' => '',
      '#default_value' => $mosparo_connection->getMosparoPublicKey(),
      '#required' => TRUE,
    ];

    $form['mosparoPrivateKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('mosparo Private key'),
      '#description' => '',
      '#default_value' => $mosparo_connection->getMosparoPrivateKey(),
      '#required' => TRUE,
    ];

    $form['verifySsl'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Verify SSL'),
      '#description' => '',
      '#default_value' => $mosparo_connection->shouldVerifySsl(),
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\mosparo_integration\MosparoConnectionInterface $mosparo_connection */
    $mosparo_connection = $this->entity;
    $status = $mosparo_connection->save();

    if ($status == SAVED_NEW) {
      $this->messenger()->addMessage($this->t('mosparo Connection %label was created.', [
        '%label' => $mosparo_connection->getLabel(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('mosparo Connection %label was updated.', [
        '%label' => $mosparo_connection->getLabel(),
      ]));
    }
    $form_state->setRedirect('mosparo_connection.list');
  }

  /**
   * Returns TRUE, if the given id already exists.
   *
   * @param string $id
   *   The id to check for.
   *
   * @return bool
   *   TRUE, if the id already exists.
   */
  public function exist(string $id): bool {
    $entity = $this->entityTypeManager->getStorage('mosparo_connection')->getQuery()
      ->condition('id', $id)
      ->execute();

    return (bool) $entity;
  }

}
