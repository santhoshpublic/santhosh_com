<?php

namespace Drupal\mosparo_integration\Service;

use Drupal\contact\MessageForm;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldConfigBase;
use Drupal\Core\Field\TypedData\FieldItemDataDefinition;
use Drupal\Core\Form\FormState;
use Drupal\mosparo_integration\Event\MosparoIntegrationFilterFieldTypesEvent;
use Drupal\mosparo_integration\Event\MosparoIntegrationFilterFormDataEvent;
use Drupal\mosparo_integration\MosparoConnectionInterface;
use Mosparo\ApiClient\Client;

/**
 * The mosparo service.
 *
 * @package Drupal\mosparo_integration
 */
class MosparoService {

  /**
   * Creates a Client object for the given mosparo connection.
   *
   * @param \Drupal\mosparo_integration\MosparoConnectionInterface $connection
   *   The mosparo connection to create the client for.
   *
   * @return \Mosparo\ApiClient\Client
   *   The created Client for the given MosparoConnection.
   */
  public function getApiClient(MosparoConnectionInterface $connection) {
    return new Client($connection->getMosparoHost(), $connection->getMosparoPublicKey(), $connection->getMosparoPrivateKey(), [
      'verify' => $connection->shouldVerifySsl() ? TRUE : FALSE,
    ]);
  }

  /**
   * Generates the HTML code for the mosparo box for the given connection.
   *
   * @param \Drupal\mosparo_integration\MosparoConnectionInterface $connection
   *   The mosparo connection.
   * @param bool $designMode
   *   TRUE, if the box should be in the design mode.
   *
   * @return string
   *   The HTML for the mosparo box.
   */
  public function generateHtml(MosparoConnectionInterface $connection, bool $designMode = FALSE): string {
    $instanceId = uniqid();

    $html = sprintf('
            <div id="mosparo-box-%s" class="drupal-mosparo-container" data-mosparo-connection-id="%s" data-processed="false" data-design-mode="%d"></div>
        ',
        $instanceId,
        $connection->getId(),
        ($designMode) ? 1 : 0
    );

    return $html;
  }

  /**
   * Generates the structure for the 'attached' key.
   *
   * @param \Drupal\mosparo_integration\MosparoConnectionInterface $connection
   *   The mosparo connection for which the information should be generated.
   *
   * @return array
   *   The array with the attached information.
   */
  public function generateAttached(MosparoConnectionInterface $connection) {
    return [
      'html_head' => [
        [
          [
            '#tag' => 'script',
            '#attributes' => [
              'src' => $connection->getFrontendJsUrl(),
              'async' => TRUE,
              'defer' => TRUE,
            ],
          ],
          'mosparo_frontend_js',
        ],
      ],
      'drupalSettings' => [
        'mosparo_integration' => [
          $connection->getId() => [
            'host' => $connection->getMosparoHost(),
            'uuid' => $connection->getMosparoUuid(),
            'publicKey' => $connection->getMosparoPublicKey(),
          ],
        ],
      ],
      'library' => [
        'mosparo_integration/mosparo_integration.frontend',
      ],
    ];
  }

  /**
   * Verifies the submitted data with the mosparo installation..
   *
   * @param array $data
   *   An array with the submitted data.
   * @param array $elements
   *   An array with the form elements.
   * @param array $additionallyIgnoredFields
   *   An array with field keys which should be ignored.
   *
   * @return array
   *   An array with the cleaned data, the required and verifiable fields,
   *   as well as the submit and validation token
   */
  public function prepareFormData(array $data, array $elements, array $additionallyIgnoredFields = []): array {
    $eventDispatcher = \Drupal::service('event_dispatcher');

    $cleanedData = [];
    $requiredFields = [];
    $verifiableFields = [];

    $ignoredKeys = array_merge(['op', 'form_build_id', 'form_id'], $additionallyIgnoredFields);
    $ignoredFieldTypes = [
      'password',
      'actions',
      'hidden',
      'captcha',
      'container',
      'webform_actions',
    ];
    $verifiableFieldTypes = [
      'textfield',
      'textarea',
      'email',
      'url',
      'string',
    ];

    // Dispatch the event to allow modules to filter the list of ignored
    // and verifiable fields.
    $event = new MosparoIntegrationFilterFieldTypesEvent($ignoredFieldTypes, $verifiableFieldTypes);
    $eventDispatcher->dispatch($event, $event::EVENT_NAME);
    $ignoredFieldTypes = $event->getIgnoredFieldTypes();
    $verifiableFieldTypes = $event->getVerifiableFieldTypes();

    $data = $this->prepareFlatStructure($data, NULL);

    foreach ($elements as $key => $el) {
      if (substr($key, 0, 1) === '#') {
        continue;
      }

      $type = $el['type'];
      if (in_array($type, $ignoredFieldTypes) || !isset($data[$key]) || in_array($key, $ignoredKeys)) {
        continue;
      }

      $cleanedData[$key] = $data[$key];

      if ($el['required']) {
        $requiredFields[] = $key;
      }

      if (in_array($type, $verifiableFieldTypes)) {
        $verifiableFields[] = $key;
      }
    }

    $submitToken = $data['_mosparo_submitToken'] ?? '';
    $validationToken = $data['_mosparo_validationToken'] ?? '';

    // Dispatch the event to allow other modules to adjust the cleaned data
    // as well as the required and verifiable fields.
    $event = new MosparoIntegrationFilterFormDataEvent($cleanedData, $requiredFields, $verifiableFields);
    $eventDispatcher->dispatch($event, $event::EVENT_NAME);

    return [
      $event->getFormData(),
      $event->getRequiredFields(),
      $event->getVerifiableFields(),
      $submitToken,
      $validationToken,
    ];
  }

  /**
   * Converts a multidimensional structure into a one dimensional.
   *
   * @param iterable $val
   *   The iterable element to iterate over it.
   * @param string|null $prefix
   *   The prefix for the key.
   *
   * @return array
   *   The prepared one-dimensional array.
   */
  protected function prepareFlatStructure(iterable $val, ?string $prefix): array {
    $data = [];

    foreach ($val as $key => $sVal) {
      if ($prefix) {
        $keys = [$prefix . '[' . $key . ']'];
        if (is_numeric($key)) {
          $keys[] = $prefix . '[]';
        }
      }
      else {
        $keys = [$key];
      }

      foreach ($keys as $sKey) {
        if (is_iterable($sVal)) {
          $data = array_merge($data, $this->prepareFlatStructure($sVal, $sKey));
        }
        else {
          $data[$sKey] = $sVal;
        }
      }
    }

    return $data;
  }

  /**
   * Extracts the field information from the form structure.
   *
   * @param \Drupal\Core\Form\FormState $formState
   *   The FormState object.
   * @param array $completeForm
   *   An array with the complete form, if available.
   *
   * @return array
   *   The elements of the form.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function extractFieldInformation(FormState $formState, array $completeForm = []): array {
    $storage = $formState->getStorage();
    $formDisplay = $storage['form_display'] ?? FALSE;

    $fieldInformation = [];

    if ($formState->getCompleteForm()) {
      $completeForm = $formState->getCompleteForm();
    }

    $elements = [];
    if ($completeForm && isset($completeForm['elements'])) {
      $elements = $completeForm['elements'];
    }

    /** @var \Drupal\contact\MessageForm $co */
    $buildInfo = $formState->getBuildInfo();
    $co = $buildInfo['callback_object'] ?? NULL;

    if ($co instanceof MessageForm) {
      // If it is a contact module form.
      $entity = $co->getEntity();

      foreach ($formDisplay->getComponents() as $key => $field) {
        if (!$formDisplay->getRenderer($key)) {
          continue;
        }

        $items = $entity->get($key);
        $items->filterEmptyItems();

        /** @var \Drupal\Core\Field\FieldItemList $items */
        if ($items->count()) {
          $items = $items->first();
        }

        $type = NULL;
        if ($items->getDataDefinition() instanceof FieldConfigBase || $items->getDataDefinition() instanceof BaseFieldDefinition) {
          $type = $items->getDataDefinition()->getType();
        }
        elseif ($items->getDataDefinition() instanceof FieldItemDataDefinition) {
          $type = $items->getDataDefinition()->getFieldDefinition()->getType();
        }

        $fullName = $key;
        if ($type === 'link') {
          $fullNameUri = $fullName . '[0][uri]';
          $fieldInformation[$fullNameUri] = [
            'required' => $items->getDataDefinition()->isRequired(),
            'type' => $this->convertDataType($type),
          ];

          $fullNameTitle = $fullName . '[0][title]';
          $fieldInformation[$fullNameTitle] = [
            'required' => $items->getDataDefinition()->isRequired(),
            'type' => $this->convertDataType('textfield'),
          ];
        }
        else {
          $formData = $completeForm[$key] ?? FALSE;
          if ($formData && isset($formData['widget']) && count($formData['widget'])) {
            $fullName .= '[0][value]';
          }

          $fieldInformation[$fullName] = [
            'required' => $items->getDataDefinition()->isRequired(),
            'type' => $this->convertDataType($type),
          ];
        }
      }
    }
    elseif ($elements) {
      // If it is a Webform module form.
      foreach ($elements as $elementKey => $element) {
        if (substr($elementKey, 0, 1) === '#') {
          continue;
        }

        $type = $this->convertDataType($element['#type'] ?? NULL);
        if (!$type) {
          continue;
        }

        $fieldInformation[$elementKey] = [
          'required' => $element['#required'],
          'type' => $type,
        ];
      }
    }

    // Add other fields if they're not already in the list.
    foreach ($completeForm as $fieldKey => $field) {
      if (isset($field['widget']) && count($field['widget'])) {
        $fieldKey .= '[0][value]';
      }

      if (substr($fieldKey, 0, 1) === '#' || isset($fieldInformation[$fieldKey])) {
        continue;
      }

      $type = $this->convertDataType($field['#type'] ?? NULL);
      if (!$type) {
        continue;
      }

      $fieldInformation[$fieldKey] = [
        'required' => $field['#required'],
        'type' => $type,
      ];
    }

    return $fieldInformation;
  }

  /**
   * Converts different string data types to 'string'.
   *
   * @param string|null $type
   *   The original data type.
   *
   * @return string
   *   The converted data type.
   */
  protected function convertDataType(?string $type): string {
    if ($type === NULL) {
      return '';
    }

    if (strpos($type, 'string') === 0) {
      return 'string';
    }

    return $type;
  }

}
