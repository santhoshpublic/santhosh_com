<?php

namespace Drupal\mosparo_integration;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Entity Interface for the MosparoConnection entity.
 *
 * @package Drupal\mosparo_integration
 */
interface MosparoConnectionInterface extends ConfigEntityInterface {

  /**
   * Returns the id.
   *
   * @return string
   *   The id of the connection.
   */
  public function getId(): string;

  /**
   * Sets the id of the connection.
   *
   * @param string $id
   *   The id.
   */
  public function setId(string $id);

  /**
   * Returns the label of the connection.
   *
   * @return string
   *   The label.
   */
  public function getLabel(): string;

  /**
   * Sets the label of the connection.
   *
   * @param string $label
   *   The label.
   */
  public function setLabel(string $label);

  /**
   * Returns the mosparo host of the connection.
   *
   * @return string
   *   The mosparo host.
   */
  public function getMosparoHost(): string;

  /**
   * Sets the mosparo host of the connection.
   *
   * @param string $mosparoHost
   *   The mosparo host.
   */
  public function setMosparoHost(string $mosparoHost);

  /**
   * Returns the mosparo UUID of the connection.
   *
   * @return string
   *   The mosparo UUID.
   */
  public function getMosparoUuid(): string;

  /**
   * Sets the mosparo UUID of the connection.
   *
   * @param string $mosparoUuid
   *   The mosparo UUID.
   */
  public function setMosparoUuid(string $mosparoUuid);

  /**
   * Returns the public key of the connection.
   *
   * @return string
   *   The mosparo public key.
   */
  public function getMosparoPublicKey(): string;

  /**
   * Sets the mosparo public key of the connection.
   *
   * @param string $mosparoPublicKey
   *   The mosparo public key.
   */
  public function setMosparoPublicKey(string $mosparoPublicKey);

  /**
   * Returns the mosparo private key of the connection.
   *
   * @return string
   *   The mosparo private key.
   */
  public function getMosparoPrivateKey(): string;

  /**
   * Sets the mosparo private key of the connection.
   *
   * @param string $mosparoPrivateKey
   *   The mosparo private key.
   */
  public function setMosparoPrivateKey(string $mosparoPrivateKey);

  /**
   * Returns TRUE, if the API client should verify SSL.
   *
   * @return bool
   *   TRUE if the SSL certificate should be verified.
   */
  public function shouldVerifySsl(): bool;

  /**
   * Enables or disabled the SSL certificate verification.
   *
   * @param bool $verifySsl
   *   TRUE for verify the SSL certificate.
   */
  public function setVerifySsl(bool $verifySsl);

}
