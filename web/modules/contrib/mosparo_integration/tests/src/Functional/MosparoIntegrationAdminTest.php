<?php

namespace Drupal\Tests\mosparo_integration\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\mosparo_integration\Entity\MosparoConnection;

/**
 * Tests the mosparo integration admin interface.
 *
 * @group mosparo_integration
 */
class MosparoIntegrationAdminTest extends MosparoIntegrationWebTestBase {

  use StringTranslationTrait;

  /**
   * Returns the mosparo connection object for the given id, or null.
   *
   * @param string $id
   *   The id of the connection.
   *
   * @return \Drupal\mosparo_integration\MosparoConnectionInterface|null
   *   The found mosparo connection object or null.
   */
  protected function getMosparoConnection(string $id) {
    $ids = \Drupal::entityQuery('mosparo_connection')
      ->condition('id', $id)
      ->execute();
    return ($ids) ? MosparoConnection::load(reset($ids)) : NULL;
  }

  /**
   * Tests the modparo connection administration UI.
   */
  public function testMosparoConnectionAdministration() {
    $connection_id = 'mosparo_test_connection';
    $connection_label = 'mosparo Test Connection';
    $connection_mosparo_host = 'https://mosparo.local';
    $connection_mosparo_uuid = '12345678-test-test-test-test12345678';
    $connection_mosparo_public_key = 'testPublicKey';
    $connection_mosparo_private_key = 'testPrivateKey';
    $connection_verify_ssl = FALSE;

    // Log in as admin.
    $this->drupalLogin($this->adminUser);

    // Try to create the connection without label.
    $form_values = [
      'mosparoHost' => $connection_mosparo_host,
      'mosparoUuid' => $connection_mosparo_uuid,
      'mosparoPublicKey' => $connection_mosparo_public_key,
      'mosparoPrivateKey' => $connection_mosparo_private_key,
      'verifySsl' => $connection_verify_ssl,
    ];
    $this->drupalGet(self::ADMIN_URL . '/add');
    $this->submitForm($form_values, 'Save');
    $this->assertSession()->pageTextContains($this->t('Label field is required.'));

    // Add the label and the id.
    $form_values['label'] = $connection_label;
    $form_values['id'] = $connection_id;
    $this->drupalGet(self::ADMIN_URL . '/add');
    $this->submitForm($form_values, $this->t('Save'));
    $this->assertSession()->responseContains($this->t('mosparo Connection %label was created.', ['%label' => $connection_label]));

    // Check in database.
    /** @var \Drupal\mosparo_integration\Entity\MosparoConnection con */
    $con = $this->getMosparoConnection($connection_id);
    $this->assertEquals($con->getMosparoHost(), $connection_mosparo_host);

    // Update the host.
    $new_mosparo_host = 'https://mosparo-new.local';
    $form_values['mosparoHost'] = $new_mosparo_host;
    $this->drupalGet(self::ADMIN_URL . '/' . $connection_id);
    $this->submitForm($form_values, $this->t('Save'));
    $this->assertSession()->responseContains($this->t('mosparo Connection %label was updated.', ['%label' => $connection_label]));

    // Check in database.
    /** @var \Drupal\mosparo_integration\Entity\MosparoConnection con */
    $con = $this->getMosparoConnection($connection_id);
    $this->assertEquals($con->getMosparoHost(), $new_mosparo_host);

    // Delete the mosparo connection.
    $this->drupalGet(self::ADMIN_URL . '/' . $connection_id . '/delete');
    $this->submitForm([], $this->t('Delete'));
    $this->assertSession()->responseContains($this->t('mosparo Connection %label has been deleted.', ['%label' => $connection_label]));

    $con = $this->getMosparoConnection($connection_id);
    $this->assertNull($con);
  }

}
