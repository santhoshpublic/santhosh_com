<?php

namespace Drupal\Tests\mosparo_integration\Unit\Service;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\contact\Entity\Message;
use Drupal\contact\MessageForm;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Field\FieldConfigBase;
use Drupal\Core\Field\FieldDefinition;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\TypedData\FieldItemDataDefinition;
use Drupal\Core\Form\FormState;
use Drupal\mosparo_integration\Entity\MosparoConnection;
use Drupal\mosparo_integration\Event\MosparoIntegrationFilterFieldTypesEvent;
use Drupal\mosparo_integration\Event\MosparoIntegrationFilterFormDataEvent;
use Drupal\mosparo_integration\MosparoConnectionInterface;
use Drupal\mosparo_integration\Service\MosparoService;
use Mosparo\ApiClient\Client;
use Mosparo\ApiClient\VerificationResult;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests the mosparo service.
 *
 * @coversDefaultClass \Drupal\mosparo_integration\Service\MosparoService
 *
 * @group mosparo_integration
 */
class MosparoServiceTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * The mosparo connection to test with.
   *
   * @var \Drupal\mosparo_integration\MosparoConnectionInterface
   */
  protected MosparoConnectionInterface $mosparoConnection;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    $eventDispatcher = $this->prophesize(ContainerAwareEventDispatcher::class);
    $eventDispatcher
      ->dispatch(Argument::type(MosparoIntegrationFilterFieldTypesEvent::class), 'mosparo_integration.filter_field_types')
      ->willReturnArgument(0);
    $eventDispatcher
      ->dispatch(Argument::type(MosparoIntegrationFilterFormDataEvent::class), 'mosparo_integration.filter_form_data')
      ->willReturnArgument(0);

    $mockContainer = $this->prophesize(ContainerInterface::class);
    $mockContainer->get('event_dispatcher')->willReturn($eventDispatcher->reveal());

    \Drupal::setContainer($mockContainer->reveal());

    $client = $this->prophesize(Client::class);
    $client
      ->verifySubmission(
        [
          'name' => 'test-name',
          'first_name[][value]' => 'test',
          'first_name[0][value]' => 'test',
        ],
        'submitToken',
        'validationToken'
      )
      ->willReturn(new VerificationResult(
        TRUE,
        TRUE,
        [
          'name' => VerificationResult::FIELD_VALID,
          'first_name[][value]' => VerificationResult::FIELD_NOT_VERIFIED,
          'first_name[0][value]' => VerificationResult::FIELD_VALID,
        ],
        []
      ));
    $this->client = $client->reveal();

    $this->mosparoConnection = new MosparoConnection([], '');
    $this->mosparoConnection->setId('mosparo-test');
    $this->mosparoConnection->setMosparoHost('https://mosparo.local');
    $this->mosparoConnection->setMosparoUuid('12345678-test-test-test-test12345678');
    $this->mosparoConnection->setMosparoPublicKey('testPublicKey');
    $this->mosparoConnection->setMosparoPrivateKey('testPrivateKey');
    $this->mosparoConnection->setVerifySsl(FALSE);

    $this->service = new MosparoService();
  }

  /**
   * Tests the service method to get an ApiClient object.
   *
   * @covers ::getApiClient
   */
  public function testGetClient() {
    $this->assertInstanceOf(Client::class, $this->service->getApiClient($this->mosparoConnection));
  }

  /**
   * Tests the service method to generate the HTML code for the mosparo box.
   *
   * @covers ::generateHtml
   */
  public function testGenerateHtml() {
    $html = $this->service->generateHtml($this->mosparoConnection);

    $this->assertMatchesRegularExpression(
      '/<div id="mosparo-box-([a-z0-9]+)" class="drupal-mosparo-container" data-mosparo-connection-id="mosparo-test" data-processed="false" data-design-mode="0"><\/div>/',
      $html
    );
  }

  /**
   * Tests the method to generate the HTML code but with design mode enabled.
   *
   * @covers ::generateHtml
   */
  public function testGenerateHtmlDesignMode() {
    $html = $this->service->generateHtml($this->mosparoConnection, TRUE);

    $this->assertMatchesRegularExpression(
      '/<div id="mosparo-box-([a-z0-9]+)" class="drupal-mosparo-container" data-mosparo-connection-id="mosparo-test" data-processed="false" data-design-mode="1"><\/div>/',
      $html
    );
  }

  /**
   * Tests the method generate the array with the attached configuration.
   *
   * @covers ::generateAttached
   */
  public function testGenerateAttached() {
    $struct = $this->service->generateAttached($this->mosparoConnection);

    $this->assertArrayHasKey('html_head', $struct);
    $this->assertArrayHasKey('drupalSettings', $struct);
    $this->assertArrayHasKey('library', $struct);

    $expectedHtmlHead = [
      [
        [
          '#tag' => 'script',
          '#attributes' => [
            'src' => 'https://mosparo.local/build/mosparo-frontend.js',
            'async' => TRUE,
            'defer' => TRUE,
          ],
        ],
        'mosparo_frontend_js',
      ],
    ];
    $this->assertEquals($expectedHtmlHead, $struct['html_head']);

    $expectedDrupalSettings = [
      'mosparo_integration' => [
        'mosparo-test' => [
          'host' => 'https://mosparo.local',
          'uuid' => '12345678-test-test-test-test12345678',
          'publicKey' => 'testPublicKey',
        ],
      ],
    ];
    $this->assertEquals($expectedDrupalSettings, $struct['drupalSettings']);

    $expectedLibrary = [
      'mosparo_integration/mosparo_integration.frontend',
    ];
    $this->assertEquals($expectedLibrary, $struct['library']);
  }

  /**
   * Tests the service method to verify a submission.
   *
   * @covers ::prepareFormData
   * @covers ::prepareFlatStructure
   */
  public function testPrepareFormData() {
    [
      $cleanedData,
      $requiredFields,
      $verifiableFields,
      $mosparoSubmitToken,
      $mosparoValidationToken,
    ] = $this->service->prepareFormData(
      [
        'name' => 'test-name',
        'first_name' => [
          [
            'value' => 'test',
          ],
        ],
        'last_name' => 'last name',
        'address_lines' => [
          'line 1',
          'line 2',
        ],
        '_mosparo_submitToken' => 'submitToken',
        '_mosparo_validationToken' => 'validationToken',
        'op' => 'ignored',
      ],
      [
        'name' => [
          'type' => 'textfield',
          'required' => FALSE,
        ],
        'first_name[0][value]' => [
          'type' => 'textfield',
          'required' => TRUE,
        ],
        'last_name' => [
          'type' => 'hidden',
          'required' => TRUE,
        ],
        'address_lines' => [
          'type' => 'textfield',
          'required' => FALSE,
        ],
        '#action' => [
          'type' => 'hidden',
          'required' => FALSE,
        ],
      ]
    );

    $targetFormData = [
      'name' => 'test-name',
      'first_name[0][value]' => 'test',
    ];

    $this->assertIsArray($cleanedData);
    $this->assertEquals($targetFormData, $cleanedData);
    $this->assertContains('first_name[0][value]', $requiredFields);
    $this->assertNotContains('last_name', $requiredFields);
    $this->assertNotContains('name', $requiredFields);
    $this->assertContains('first_name[0][value]', $verifiableFields);
    $this->assertNotContains('last_name', $verifiableFields);
    $this->assertContains('name', $verifiableFields);
    $this->assertEquals('submitToken', $mosparoSubmitToken);
    $this->assertEquals('validationToken', $mosparoValidationToken);
  }

  /**
   * Tests the service method to get all field information (Module: Captcha).
   *
   * @covers ::extractFieldInformation
   * @covers ::convertDataType
   */
  public function testExtractFieldInformationCaptcha() {
    $formState = new FormState();

    $fieldInformation = $this->service->extractFieldInformation($formState, [
      '#required' => FALSE,
      'name' => [
        '#type' => 'string-long',
        '#required' => TRUE,
        'widget' => [
          [
            // Empty.
          ],
        ],
      ],
      'country' => [
        '#type' => 'textfield',
        '#required' => FALSE,
      ],
      'street' => [
        '#type' => NULL,
      ],
    ]);

    $target = [
      'name[0][value]' => [
        'required' => TRUE,
        'type' => 'string',
      ],
      'country' => [
        'required' => FALSE,
        'type' => 'textfield',
      ],
    ];

    $this->assertIsArray($fieldInformation);
    $this->assertEquals($target, $fieldInformation);
  }

  /**
   * Tests the service method to get all field information (Module: Webform).
   *
   * @covers ::extractFieldInformation
   * @covers ::convertDataType
   */
  public function testExtractFieldInformationWebform() {
    $completeForm = [
      'elements' => [
        '#count' => 3,
        'name' => [
          '#type' => 'string-long',
          '#required' => TRUE,
          'widget' => [
            [
              // Empty.
            ],
          ],
        ],
        'country' => [
          '#type' => 'textfield',
          '#required' => FALSE,
        ],
        'street' => [
          '#type' => NULL,
        ],
      ],
    ];

    $formState = new FormState();
    $formState->setCompleteForm($completeForm);

    $fieldInformation = $this->service->extractFieldInformation($formState);

    $target = [
      'name' => [
        'required' => TRUE,
        'type' => 'string',
      ],
      'country' => [
        'required' => FALSE,
        'type' => 'textfield',
      ],
    ];

    $this->assertIsArray($fieldInformation);
    $this->assertEquals($target, $fieldInformation);
  }

  /**
   * Tests the service method to get all field information (Module: Contact).
   *
   * @covers ::extractFieldInformation
   * @covers ::convertDataType
   */
  public function testExtractFieldInformationContact() {
    $nameDataDefinition = $this->prophesize(FieldConfigBase::class);
    $nameDataDefinition
      ->getType()
      ->willReturn('string-long');
    $nameDataDefinition
      ->isRequired()
      ->willReturn(TRUE);

    $countryFieldDefinition = $this->prophesize(FieldDefinition::class);
    $countryFieldDefinition
      ->getType()
      ->willReturn('textfield');

    $countryDataDefinition = $this->prophesize(FieldItemDataDefinition::class);
    $countryDataDefinition
      ->getFieldDefinition()
      ->willReturn($countryFieldDefinition->reveal());
    $countryDataDefinition
      ->isRequired()
      ->willReturn(FALSE);

    $websiteDataDefinition = $this->prophesize(FieldConfigBase::class);
    $websiteDataDefinition
      ->getType()
      ->willReturn('link');
    $websiteDataDefinition
      ->isRequired()
      ->willReturn(FALSE);

    $nameItemList = $this->prophesize(FieldItemList::class);
    $nameItemList
      ->getDataDefinition()
      ->willReturn($nameDataDefinition->reveal());

    $nameItemListMulti = $this->prophesize(FieldItemList::class);
    $nameItemListMulti
      ->filterEmptyItems()
      ->willReturn($this->anything());
    $nameItemListMulti
      ->count()
      ->willReturn(1);
    $nameItemListMulti
      ->first()
      ->willReturn($nameItemList->reveal());

    $countryItemList = $this->prophesize(FieldItemList::class);
    $countryItemList
      ->filterEmptyItems()
      ->willReturn($this->anything());
    $countryItemList
      ->count()
      ->willReturn(0);
    $countryItemList
      ->getDataDefinition()
      ->willReturn($countryDataDefinition->reveal());

    $websiteItemList = $this->prophesize(FieldItemList::class);
    $websiteItemList
      ->filterEmptyItems()
      ->willReturn($this->anything());
    $websiteItemList
      ->count()
      ->willReturn(0);
    $websiteItemList
      ->getDataDefinition()
      ->willReturn($websiteDataDefinition->reveal());

    $message = $this->prophesize(Message::class);
    $message
      ->get('name')
      ->willReturn($nameItemListMulti->reveal());

    $message
      ->get('country')
      ->willReturn($countryItemList->reveal());

    $message
      ->get('website')
      ->willReturn($websiteItemList->reveal());

    $messageForm = $this->prophesize(MessageForm::class);
    $messageForm
      ->getEntity()
      ->willReturn($message->reveal());

    $formDisplay = $this->prophesize(EntityFormDisplay::class);
    $formDisplay
      ->getComponents()
      ->willReturn([
        'name' => [
          '#required' => TRUE,
        ],
        'street' => [
          // Empty.
        ],
        'country' => [
          '#required' => FALSE,
        ],
        'website' => [
          '#required' => FALSE,
        ],
      ]);

    $formDisplay
      ->getRenderer('name')
      ->willReturn(TRUE);

    $formDisplay
      ->getRenderer('country')
      ->willReturn(TRUE);

    $formDisplay
      ->getRenderer('website')
      ->willReturn(TRUE);

    $formDisplay
      ->getRenderer('street')
      ->willReturn(FALSE);

    $buildInfo = [
      'callback_object' => $messageForm->reveal(),
    ];

    $storage = [
      'form_display' => $formDisplay->reveal(),
    ];

    $formState = new FormState();
    $formState->setBuildInfo($buildInfo);
    $formState->setSTorage($storage);

    $fieldInformation = $this->service->extractFieldInformation($formState, [
      'name' => [
        'widget' => [
          [
            // Empty.
          ],
        ],
      ],
    ]);

    $target = [
      'name[0][value]' => [
        'required' => TRUE,
        'type' => 'string',
      ],
      'country' => [
        'required' => FALSE,
        'type' => 'textfield',
      ],
      'website[0][uri]' => [
        'required' => FALSE,
        'type' => 'link',
      ],
      'website[0][title]' => [
        'required' => FALSE,
        'type' => 'textfield',
      ],
    ];

    $this->assertIsArray($fieldInformation);
    $this->assertEquals($target, $fieldInformation);
  }

}
