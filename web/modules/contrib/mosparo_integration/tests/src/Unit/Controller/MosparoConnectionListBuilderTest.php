<?php

namespace Drupal\Tests\mosparo_integration\Unit\Controller;

use Drupal\mosparo_integration\Controller\MosparoConnectionListBuilder;
use Drupal\mosparo_integration\Entity\MosparoConnection;
use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests the list builder for the mosparo connection list.
 *
 * @coversDefaultClass \Drupal\mosparo_integration\Controller\MosparoConnectionListBuilder
 *
 * @group mosparo_integration
 */
class MosparoConnectionListBuilderTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    $this->mockModuleHandler = $this->prophesize(ModuleHandlerInterface::class);
    $this->mockModuleHandler->invokeAll(Argument::any(), Argument::any())->willReturn([]);
    $this->mockModuleHandler->alter(Argument::any(), Argument::any(), Argument::any())->willReturn([]);

    $this->mockContainer = $this->prophesize(ContainerInterface::class);
    $this->mockContainer->get('string_translation')->willReturn($this->getStringTranslationStub());
    $this->mockContainer->get('module_handler')->willReturn($this->mockModuleHandler->reveal());

    $this->mockEntityType = $this->prophesize(EntityTypeInterface::class);
    $this->mockEntityStorage = $this->prophesize(EntityStorageInterface::class);
    $this->listBuilder = new MosparoConnectionListBuilder($this->mockEntityType->reveal(), $this->mockEntityStorage->reveal());

    \Drupal::setContainer($this->mockContainer->reveal());
  }

  /**
   * Tests the method to build the list header.
   *
   * @covers ::buildHeader
   */
  public function testBuildHeader() {
    $header = $this->listBuilder->buildHeader();
    $this->assertArrayHasKey('label', $header);
    $this->assertArrayHasKey('mosparoHost', $header);
  }

  /**
   * Tests the method to build the list row.
   *
   * @covers ::buildRow
   */
  public function testBuildRow() {
    $mockEntity = $this->prophesize(MosparoConnection::class);
    $mockEntity->access(Argument::any())->willReturn(FALSE);
    $mockEntity->id()->willReturn('test_connection');
    $mockEntity->getLabel()->willReturn('test Connection');
    $mockEntity->getMosparoHost()->willReturn('https://mosparo.local');
    $mockEntity->hasLinkTemplate('edit-form')->willReturn(FALSE);
    $mockEntity->hasLinkTemplate('delete-form')->willReturn(FALSE);

    $row = $this->listBuilder->buildRow($mockEntity->reveal());

    $this->assertArrayHasKey('label', $row);
    $this->assertEquals('test Connection', $row['label']);

    $this->assertArrayHasKey('mosparoHost', $row);
    $this->assertEquals('https://mosparo.local', $row['mosparoHost']);
  }

}
