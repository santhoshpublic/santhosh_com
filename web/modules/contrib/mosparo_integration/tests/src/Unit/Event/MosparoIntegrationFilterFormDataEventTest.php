<?php

namespace Drupal\Tests\mosparo_integration\Unit\Event;

use Drupal\mosparo_integration\Event\MosparoIntegrationFilterFormDataEvent;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the mosparo Filter Form Data Event.
 *
 * @coversDefaultClass \Drupal\mosparo_integration\Event\MosparoIntegrationFilterFormDataEvent
 *
 * @group mosparo_integration
 */
class MosparoIntegrationFilterFormDataEventTest extends UnitTestCase {

  /**
   * Tests the setters and getters.
   */
  public function testEvent() {
    $formData = ['name' => 'test', 'street' => 'Teststreet'];
    $requiredFields = ['name'];
    $verifiableFields = ['name', 'street'];

    $event = new MosparoIntegrationFilterFormDataEvent($formData, $requiredFields, $verifiableFields);
    $this->assertEquals($event->getFormData(), $formData);
    $this->assertEquals($event->getRequiredFields(), $requiredFields);
    $this->assertEquals($event->getVerifiableFields(), $verifiableFields);

    $newFormData = array_merge($formData, ['city' => 'Testcity']);
    $newRequiredFields = array_merge($requiredFields, ['city']);
    $newVerifiableFields = array_merge($verifiableFields, ['city']);

    $event->setFormData($newFormData);
    $event->setRequiredFields($newRequiredFields);
    $event->setVerifiableFields($newVerifiableFields);

    $this->assertEquals($event->getFormData(), $newFormData);
    $this->assertEquals($event->getRequiredFields(), $newRequiredFields);
    $this->assertEquals($event->getVerifiableFields(), $newVerifiableFields);
  }

}
