<?php

namespace Drupal\Tests\mosparo_integration\Unit\Entity;

use Drupal\mosparo_integration\Entity\MosparoConnection;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the mosparo connection entity.
 *
 * @coversDefaultClass \Drupal\mosparo_integration\Entity\MosparoConnection
 *
 * @group mosparo_integration
 */
class MosparoConnectionTest extends UnitTestCase {

  /**
   * Tests the setters and getters.
   */
  public function testMosparoConnection() {
    $con = new MosparoConnection([], '');
    $con->setId('mosparo-test');
    $con->setLabel('mosparo Test');
    $con->setMosparoHost('https://mosparo.local');
    $con->setMosparoUuid('12345678-test-test-test-test12345678');
    $con->setMosparoPublicKey('testPublicKey');
    $con->setMosparoPrivateKey('testPrivateKey');
    $con->setVerifySsl(TRUE);

    $this->assertEquals($con->getId(), 'mosparo-test');
    $this->assertEquals($con->getLabel(), 'mosparo Test');
    $this->assertEquals($con->getMosparoHost(), 'https://mosparo.local');
    $this->assertEquals($con->getMosparoUuid(), '12345678-test-test-test-test12345678');
    $this->assertEquals($con->getMosparoPublicKey(), 'testPublicKey');
    $this->assertEquals($con->getMosparoPrivateKey(), 'testPrivateKey');
    $this->assertTrue($con->shouldVerifySsl());
  }

}
