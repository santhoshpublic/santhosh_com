# Entity Submenu Block

Displays the current page's submenu items as rendered content entities
(typically in the 'teaser' or similar view mode).

You can add Entity Submenu Blocks from the Block layout page and configure the
view modes that should be used for different content entity types. By default
only menu items for content entities are rendered, but there is an option to
render other menu items as simple links too.
