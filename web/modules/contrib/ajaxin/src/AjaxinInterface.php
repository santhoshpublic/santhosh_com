<?php

namespace Drupal\ajaxin;

use Drupal\blazy\BlazyInterface;

/**
 * Defines re-usable services and functions for Ajaxin plugins.
 */
interface AjaxinInterface extends BlazyInterface {

  /**
   * Returns available CSS skin components.
   */
  public function getSkins();

  /**
   * Implements hook_library_info_build().
   */
  public function libraryInfoBuild();

  /**
   * Implements hook_library_info_alter().
   */
  public function libraryInfoAlter(array &$libraries, $module);

  /**
   * Implements hook_page_attachments().
   */
  public function pageAttachments(array &$page);

}
