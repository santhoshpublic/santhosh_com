<?php

namespace Drupal\ajaxin;

use Drupal\blazy\BlazyBase;

/**
 * Provides Ajaxin utility.
 */
class Ajaxin extends BlazyBase implements AjaxinInterface {

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'ajaxin';

  /**
   * The available skins.
   *
   * @var array
   */
  protected $skins;

  /**
   * The JS config.
   *
   * @var array
   */
  protected $jsConfig;

  /**
   * The library info definition.
   *
   * @var array
   */
  protected $libraryInfoBuild;

  /**
   * Checks if the library is attached.
   *
   * @var bool
   */
  protected $pageAttachments = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getSkins() {
    if (!isset($this->skins)) {
      $skins = [
        'chasing-dots'    => 2,
        'circle'          => 12,
        'cube-grid'       => 9,
        'double-bounce'   => 2,
        'fading-circle'   => 12,
        'pulse'           => 0,
        'rotating-plane'  => 0,
        'three-bounce'    => 3,
        'wandering-cubes' => 2,
        'wave'            => 5,
        'wordpress'       => 1,
      ];

      $this->moduleHandler->alter('ajaxin_skins', $skins);
      $this->skins = $skins;
    }

    return $this->skins;
  }

  /**
   * {@inheritdoc}
   */
  public function libraryInfoBuild() {
    if (!isset($this->libraryInfoBuild)) {
      $libraries = [];
      foreach ($this->getSkins() as $skin => $count) {
        $libraries[$skin]['css']['component']['css/components/ajaxin.' . $skin . '.css'] = [];
        $libraries[$skin]['dependencies'][] = 'ajaxin/ajaxin';
      }
      $this->libraryInfoBuild = $libraries;
    }

    return $this->libraryInfoBuild;
  }

  /**
   * {@inheritdoc}
   */
  public function libraryInfoAlter(array &$libraries, $module) {
    // Adds our ajaxin to drupal.ajax or blazy.load for a custom progressbar.
    $ajax  = $module === 'core' && isset($libraries['drupal.ajax']);
    $blazy = $module === 'blazy' && isset($libraries['load']);
    $path  = $this->getPath('module', 'ajaxin');

    if ($ajax) {
      $libraries['drupal.ajax']['js']['/' . $path . '/js/ajaxin.ajax.min.js'] = ['minified' => TRUE];
    }

    if ($blazy) {
      $libraries['load']['js']['/' . $path . '/js/ajaxin.blazy.min.js'] = ['minified' => TRUE];
    }

    if ($ajax || $blazy) {
      $item = $blazy ? 'load' : 'drupal.ajax';
      $config = $this->jsConfig();

      $libraries[$item]['dependencies'][] = 'ajaxin/ajaxin';
      if (!$this->config('sitewide', 'ajaxin.settings')) {
        $libraries[$item]['dependencies'][] = 'ajaxin/' . $config['skin'];
        $libraries[$item]['drupalSettings']['ajaxin'] = $config;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function pageAttachments(array &$page) {
    if (!$this->pageAttachments) {
      $config = $this->jsConfig();
      if (!empty($config['sitewide'])) {
        $page['#attached']['library'][] = 'ajaxin/' . $config['skin'];
        $page['#attached']['drupalSettings']['ajaxin'] = $config;
      }
      $this->pageAttachments = TRUE;
    }
  }

  /**
   * Returns the Ajaxin config.
   */
  protected function jsConfig() {
    if (!isset($this->jsConfig)) {
      $config = $this->config('', 'ajaxin.settings');
      $config['count'] = 0;
      if ($skin = $config['skin']) {
        $config['count'] = $this->getSkins()[$skin];

        // Provides the inline Ajaxin theme.
        $template = ['#theme' => 'ajaxin'];
        $config['theme'] = $this->renderer->renderPlain($template);

        // Provides the full screen Ajaxin theme.
        $template = ['#theme' => 'ajaxin', '#fs' => TRUE];
        $config['themeFullscreen'] = $this->renderer->renderPlain($template);
      }
      $this->jsConfig = $config;
    }

    return $this->jsConfig;
  }

}
