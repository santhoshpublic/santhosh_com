<?php

/**
 * @file
 * Breadcrumb Manager API documentation.
 */

/**
 * @addtogroup hooks
 * @{
 */

use Drupal\Core\Url;

/**
 * Implements hook_breadcrumb_manager_fake_segments_alter().
 */
function hook_breadcrumb_manager_fake_segments_alter($current_path, &$title, Url &$url) {
  $mapping = [
    '/fake-segment-news' => [
      'title' => 'My amazing news',
      'path' => '/news',
    ],
  ];

  if (isset($mapping[$current_path])) {
    $title = $mapping[$current_path]['title'];
    $url = Url::fromUserInput($mapping[$current_path]['path']);
  }
}
