<?php

namespace Drupal\outlayer\Plugin\gridstack\engine;

use Drupal\gridstack\Plugin\gridstack\engine\GridStackJs;

/**
 * Provides an Outlayer UnGridStack JS layout engine.
 *
 * Applies to all layouts, Masonry/Packer/Isotope, if ungridstack and not
 * using native CSS Grid layout. This layout is loaded if Grid Custom option is
 * provided which disables GridStack layouts/ optionsets.
 *
 * @GridStackEngine(
 *   id = "outlayer_ungridstack",
 *   group = "outlayer",
 *   hidden = "true",
 *   version = "1",
 *   label = @Translation("Outlayer UnGridStack")
 * )
 */
class OutlayerUnGridStack extends GridStackJs {

  /**
   * {@inheritdoc}
   */
  protected $containerClasses = [
    'gridstack--gs',
    'is-gs-enabled',
    'is-gs-layout',
    'is-gs-packing',
  ];

  /**
   * UnGridStack layout doesn't need DOM rect positions, just dimensions.
   *
   * {@inheritdoc}
   */
  protected $nodes = ['width', 'height'];

  /**
   * {@inheritdoc}
   */
  public function attach(array &$load, array $attach, $config): void {
    parent::attach($load, $attach, $config);

    $load['library'][] = 'outlayer/ungridstack';
  }

  /**
   * {@inheritdoc}
   */
  protected function itemAttributes(array &$attributes, array &$settings) {
    if ($dimensions = $this->dimensions($settings)) {
      $settings['_grids'] = $dimensions;
    }

    parent::itemAttributes($attributes, $settings);
  }

  /**
   * Returns custom grids containing dimensions only without DOM rect.
   */
  protected function dimensions(array &$settings) {
    $data = [];
    if ($dimensions = $this->getSetting('dimensions')) {
      // $config = $settings['gridstacks'] ?? NULL;
      $delta = $settings['delta'] ?? 0;
      $count = $settings['dimensions_count'] - 1;

      // @todo recheck.
      // if ($config) {
      // $delta = $config->get('delta', 0) ?: $delta;
      // }
      // If not defined, at least skip the first, normally the largest box.
      $index = 0;
      if (isset($dimensions[$delta])) {
        $index = $delta;
      }
      else {
        $index = $settings['dimensions_count'] == 1 ? 0 : rand(1, $count);
        // Repeat if the grid count is 1.
        if ($settings['dimensions_count'] == 1) {
          $settings['delta'] = 0;
        }
      }

      // Outlayer Grid and Isotope have custom grids which disables gridstack.
      if (isset($dimensions[$index])) {
        $width = $dimensions[$index]['width'] ?? NULL;
        $height = $dimensions[$index]['height'] ?? NULL;

        if ($width) {
          $data[$index]['width'] = (int) $width;

          // Optional height.
          if ($height) {
            $data[$index]['height'] = (int) $height;
          }
        }
      }
    }
    return $data;
  }

}
