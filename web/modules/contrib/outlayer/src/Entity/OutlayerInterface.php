<?php

namespace Drupal\outlayer\Entity;

use Drupal\gridstack\Entity\GridStackBaseInterface;

/**
 * Provides an interface defining Outlayer entity.
 */
interface OutlayerInterface extends GridStackBaseInterface {}
