<?php

namespace Drupal\drowl_layouts\Plugin\Layout;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Layout\LayoutDefault;

/**
 * Fixed / equal column layout plugin class.
 *
 * @internal
 *   Plugin classes are internal.
 */
class DrowlLayoutsLayoutDefault extends LayoutDefault implements PluginFormInterface {
  use DrowlLayoutsSettingsTrait;

}
