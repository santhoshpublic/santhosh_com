<?php

namespace Drupal\drowl_layouts\Plugin\Layout;

/**
 * The functions that are available outside for DrowlLayoutsMultiWidthLayout.
 */
interface DrowlLayoutsMultiWidthLayoutInterface {

  /**
   * Returns the numbers of columns.
   *
   * @return int
   *   The column count.
   */
  public function getColumnCount();

}
