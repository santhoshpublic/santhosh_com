<?php

namespace Drupal\drowl_layouts\Plugin\Layout;

use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Configurable multi column layout plugin class.
 *
 * Configurable multi column layout plugin class, for all layouts > 3 columns.
 * Doesn't implement many options, because this is an edge case and would be
 * far to complicated.
 * !! Hint: If we will ever need more options, better create an individual class
 * like DrowlLayoutsThreeColumnLayout for the number of columns. !!
 *
 * @internal
 *   Plugin classes are internal.
 */
class DrowlLayoutsMultiColumnLayout extends DrowlLayoutsMultiWidthLayoutBase implements PluginFormInterface {

  /**
   * Returns the number of columns in this layout.
   *
   * !! Special case: the number is not clearly defined in this workaround class
   * !! here so we return NULL as special case.
   *
   * @var int
   */
  protected $columnCount = NULL;

  /**
   * {@inheritdoc}
   */
  public function getColumnCount() {
    return $this->columnCount;
  }

  /**
   * {@inheritdoc}
   */
  protected function getWidthOptions() {
    return [
      'fixed' => 'Fixed',
      'auto'  => 'Automatic',
    ];
  }

}
