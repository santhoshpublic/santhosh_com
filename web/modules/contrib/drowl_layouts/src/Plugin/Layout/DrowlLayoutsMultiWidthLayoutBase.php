<?php

namespace Drupal\drowl_layouts\Plugin\Layout;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\layout_builder\Plugin\Layout\MultiWidthLayoutBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Layout plugin class to provide general options for MultiWidthLayout.
 *
 * @internal
 *   Plugin classes are internal.
 */
abstract class DrowlLayoutsMultiWidthLayoutBase extends MultiWidthLayoutBase implements PluginFormInterface, DrowlLayoutsMultiWidthLayoutInterface {
  use DrowlLayoutsSettingsTrait {
    defaultConfiguration as drowlLayoutsSettingsTraitDefaultConfiguration;
    buildConfigurationForm as drowlLayoutsSettingsTraitBuildConfigurationForm;
    validateConfigurationForm as drowlLayoutsSettingsTraitValidateConfigurationForm;
    submitConfigurationForm as drowlLayoutsSettingsTraitSubmitConfigurationForm;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // Inherit from parents:
    $configuration = $this->drowlLayoutsSettingsTraitDefaultConfiguration();
    return $configuration + [
      // Set OUR defaults:
      'layout_variant' => 'card',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Inherit from parents:
    $form += parent::buildConfigurationForm($form, $form_state);
    $form += $this->drowlLayoutsSettingsTraitBuildConfigurationForm($form, $form_state);

    // Add wrapper for settings preview around the column_width field.
    // -- Column width --.
    $field_name = 'column_widths';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
      '#weight' => -1,
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_layouts_settings_preview_cell_width',
      '#field_related' => $field_name,
      // Provide the column count.
      '#column_count' => $this->getColumnCount(),
    ];
    // Move the column width into the wrapper.
    $form[$wrapper_name][$field_name] = $form[$field_name];
    // Remove it from the old position:
    unset($form[$field_name]);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $this->drowlLayoutsSettingsTraitValidateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @todo We should better massage the form values here, but neither
    // #tree = true nor other things work... So we use the nested array so far.
    // !! For this reason we have to override the parent class, which should
    // be undone as soon as we found a solution.
    // @todo This is also affected by this bug:
    // https://www.drupal.org/project/drupal/issues/3220298
    // When using the Media Browser you have to edit after first creation
    // and save again, then values are saved...
    $columnWithWrapper = $form_state->getValue('column_widths_wrapper');
    if (!empty($columnWithWrapper) && isset($columnWithWrapper['column_widths'])) {
      $form_state->setValue('column_widths', $columnWithWrapper['column_widths']);
    }
    parent::submitConfigurationForm($form, $form_state);
    $this->drowlLayoutsSettingsTraitSubmitConfigurationForm($form, $form_state);
  }

  /**
   * Returns the numbers of columns.
   *
   * @return int
   *   The column count.
   */
  abstract public function getColumnCount();

}
