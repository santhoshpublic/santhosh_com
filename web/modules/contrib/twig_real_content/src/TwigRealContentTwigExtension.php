<?php

namespace Drupal\twig_real_content;

use Twig\TwigTest;
use Twig\TwigFilter;
use Drupal\Core\Render\Element;
use Twig\Extension\AbstractExtension;
use Drupal\Component\Render\MarkupInterface;
use Drupal\twig_real_content\Exceptions\TwigRealContentException;

/**
 * Twig extension.
 */
class TwigRealContentTwigExtension extends AbstractExtension {

  /**
   * List of allowed tags.
   *
   * @var array
   */
  protected $allowedTags = [
    '<drupal-render-placeholder>',
    '<embed>',
    '<hr>',
    '<iframe>',
    '<img>',
    '<input>',
    '<link>',
    '<object>',
    '<script>',
    '<source>',
    '<style>',
    '<video>',
  ];

  /**
   * {@inheritdoc}
   */
  public function getTests() {
    return [
      new TwigTest('real_content', function ($value) {
        if (is_array($value) && Element::isEmpty($value)) {
          return FALSE;
        }
        $value = $this->ensureRendered($value);
        $value = $this->stripText($value, $this->allowedTags);
        return !empty($value);
      }),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new TwigFilter('real_content', function ($value) {
        if (is_array($value) && Element::isEmpty($value)) {
          return '';
        }
        $value = $this->ensureRendered($value);
        $value = $this->stripText($value, $this->allowedTags);
        return $value;
      }),
    ];
  }

  /**
   * Ensure $value is rendered.
   *
   * Helper function to ensure the given value is rendered to be able
   * to check its real contents.
   * See core issue [#953034] for details.
   *
   * @param mixed $value
   *
   * @return string
   *
   * @throws \Drupal\twig_real_content\Exceptions\TwigRealContentException
   */
  protected function ensureRendered($value) {
    // @see https://www.drupal.org/project/twig_real_content/issues/3293416
    // The following would allow us to render the content given,
    // but that would lead to hidden performance implications,
    // so until now we decided not to do this.
    // Perhaps we could combine the implementation with
    // https://www.drupal.org/project/twig_capture and only run it
    // if enabled later on.
    // if (is_array($value)) {
    //   // Return early if the element is empty:
    //   if (Element::isEmpty($value)) {
    //     return '';
    //   }
    //   return \Drupal::service('renderer')->render($value);
    // }

    // If the value is NULL, return an empty string instead:
    if ($value === NULL) {
      return '';
    }

    if (!is_string($value) && !$value instanceof MarkupInterface) {
      throw new TwigRealContentException('"Twig Has Content Filter" expects rendered strings as value, ' . gettype($value) . ' given. Did you forget to |render / capture the twig variable before?');
    }

    return $value;
  }

  /**
   * Helper function to strip and trim the text.
   *
   * Helps to find out if there is real content within the given text
   * or just empty wrapper markup.
   *
   * @param string $text
   * @param array $allowed_tags
   *
   * @return string
   *
   * @throws \Twig\Error\RuntimeError
   */
  protected function stripText(string $text, array $allowed_tags) {
    $text = strip_tags($text, $allowed_tags);
    $text = twig_trim_filter($text);
    return $text;
  }

}
